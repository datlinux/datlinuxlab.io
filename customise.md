---
title: Customised ISO
subtitle: Get a branded, customised data science distro
comments: false
---

___

### Customised DAT Linux ISO for your school/college/university - [open guide](/DAT-Linux-custom-ISO.pdf)

$AUD375\* (one payment)
> [Purchase here via Patreon](https://www.patreon.com/DATLinux/shop/customised-dat-linux-iso-for-your-school-73820?utm_medium=clipboard_copy&utm_source=copyLink&utm_campaign=productshare_creator&utm_content=join_link)
> OR [Purchase here via Paypal](https://py.pl/2ubUCMb9EnK)
> - A custom ISO build with your school/college/university branding
> - Private, custom named ISO download and link
> - PRO Edition tools license for 20 seats
> - Your own "tab" on the Control Panel
> - Add your own custom icons/links to the control panel
> - Priority consideration and fast-tracking of new app ideas (for universal apps)\*


*\* New apps should have a FOSS edition, be universally useful, related to data science, and feasible to include*

___ 

### Other ways to show your support:

{{< rawhtml >}}
<table>
<tr>
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license. <a href="/pro">Get PRO now!</a>
</td>
</tr>
</table>
<br/>
{{< /rawhtml >}}

{{< rawhtml >}}
<table style="width:100%;">
<tr>
<td style="width:90px;padding:5px;background-color:#e4e4e4;">
    <img style="width:90px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:5px;color:#333333;">
    <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team. <a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/"> 
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

{{< rawhtml >}}
<table>
<tr>
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;">
    <a href="/customise"><img style="width:90px" src="/datlinux-custom-iso.png" /></a>
</td>
<td style="padding:5px;color:#333333;">
    Need a customised DAT Linux ISO for your school/college/university? Get your own branded, customised data science distro <a href="/customise">here</a>.
</td>
</tr>
</table>
<br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
> 
> {{< rawhtml >}}
<div style="margin-top:20px">
👍 <a target="_blank" href="https://distrowatch.com/dwres.php?waitingdistro=676&resource=links#new" target="_blank">Recommend DAT Linux on DistroWatch</a>.
</div>
{{< /rawhtml >}}
