> [🛒️ Get this book as a PDF or EPUB e-book from Leanpub](https://leanpub.com/numpybyexample).

# Appendix C. NumPy Function & Property Reference

(appendix-c)=

Class member information listed here was extracted from the NumPy source code 'docstring', as of NumPy version: `1.26.4`. Some sub-modules have been excluded, for example `numpy.ctypeslib` and `numpy.testing`. `numpy.matrix` is no longer recommended and stands to be deprecated.

Listings of other sub-classes such as `numpy.chararray` and `numpy.bmat` were also excluded for brevity. You can see a full schedule of classes at the NumPy documentation web page[^numpyclassref].

## C.1  `numpy`

This section lists only the immediate members of this top-level NumPy class. Relevant sub-modules are listed in the remaining sections. 

### `numpy` *A — F*

| Member | Description |
|:-------|:------------|
| `abs` | Calculate the absolute value element-wise. |
| `absolute` | Calculate the absolute value element-wise. |
| `add` | Add arguments element-wise. |
| `add_docstring` | Add a docstring to a built-in obj if possible. |
| `add_newdoc` | Add documentation to an existing object, typically one defined in C |
| `add_newdoc_ufunc` | Replace the docstring for a ufunc with new_docstring. |
| `all` | Test whether all array elements along a given axis evaluate to True. |
| `allclose` | Returns True if two arrays are element-wise equal within a tolerance. |
| `alltrue` | Check if all elements of input array are true. |
| `amax` | Return the maximum of an array or maximum along an axis. |
| `amin` | Return the minimum of an array or minimum along an axis. |
| `angle` | Return the angle of the complex argument. |
| `any` | Test whether any array element along a given axis evaluates to True. |
| `append` | Append values to the end of an array. |
| `apply_along_axis` | Apply a function to 1-D slices along the given axis. |
| `apply_over_axes` | Apply a function repeatedly over multiple axes. |
| `arange` | Return evenly spaced values within a given interval. |
| `arccos` | Trigonometric inverse cosine, element-wise. |
| `arccosh` | Inverse hyperbolic cosine, element-wise. |
| `arcsin` | Inverse sine, element-wise. |
| `arcsinh` | Inverse hyperbolic sine element-wise. |
| `arctan` | Trigonometric inverse tangent, element-wise. |
| `arctan2` | Element-wise arc tangent of 'x1/x2' choosing the quadrant correctly. |
| `arctanh` | Inverse hyperbolic tangent element-wise. |
| `argmax` | Returns the indices of the maximum values along an axis. |
| `argmin` | Returns the indices of the minimum values along an axis. |
| `argpartition` | Perform an indirect partition along the given axis using the algorithm specified by the 'kind' keyword. |
| `argsort` | Returns the indices that would sort an array. |
| `argwhere` | Find the indices of array elements that are non-zero, grouped by element. |
| `around` | Round an array to the given number of decimals. |
| `array` | Create an array. |
| `array2string` | Return a string representation of an array. |
| `array_equal` | True if two arrays have the same shape and elements, False otherwise. |
| `array_equiv` | Returns True if input arrays are shape consistent and all elements equal. |
| `atleast_2d` | View inputs as arrays with at least two dimensions. |
| `atleast_3d` | View inputs as arrays with at least three dimensions. |
| `average` | Compute the weighted average along the specified axis. |
| `bartlett` | Return the Bartlett window. |
| `base_repr` | Return a string representation of a number in the given base system. |
| `binary_repr` | Return the binary representation of the input number as a string. |
| `bincount` | Count number of occurrences of each value in array of non-negative ints. |
| `bitwise_and` | Compute the bit-wise AND of two arrays element-wise. |
| `bitwise_not` | Compute bit-wise inversion, or bit-wise NOT, element-wise. |
| `bitwise_or` | Compute the bit-wise OR of two arrays element-wise. |
| `bitwise_xor` | Compute the bit-wise XOR of two arrays element-wise. |
| `blackman` | Return the Blackman window. |
| `block` | Assemble an nd-array from nested lists of blocks. |
| `bmat` | Build a matrix object from a string, nested sequence, or array. |
| `bool8` | Boolean type (True or False), stored as a byte. |
| `bool_` | Boolean type (True or False), stored as a byte. |
| `broadcast` | Produce an object that mimics broadcasting. |
| `broadcast_arrays` | Broadcast any number of arrays against each other. |
| `broadcast_shapes` | Broadcast the input shapes into a single shape. |
| `broadcast_to` | Broadcast an array to a new shape. |
| `busday_count` | Counts the number of valid days between 'begindates' and 'enddates', not including the day of 'enddates'. |
| `busday_offset` | First adjusts the date to fall on a valid day according to the 'roll' rule, then applies offsets to the given dates. |
| `busdaycalendar` | A business day calendar object that efficiently stores information |
| `byte` | Signed integer type, compatible with C 'char'. |
| `byte_bounds` | Returns pointers to the end-points of an array. |
| `bytes0` | A byte string. |
| `bytes_` | A byte string. |
| `c_` | Translates slice objects to concatenation along the second axis. |
| `can_cast` | Returns True if cast between data types can occur according to the casting rule. |
| `cast` | Base object for a dictionary for look-up with any alias for an array dtype. |
| `cbrt` | Return the cube-root of an array, element-wise. |
| `cdouble` | Complex number type composed of two double-precision floating-point |
| `ceil` | Return the ceiling of the input, element-wise. |
| `cfloat` | Complex number type composed of two double-precision floating-point numbers, compatible with Python 'complex'. |
| `char` | This module contains a set of functions for vectorized string operations and methods. |
| `character` | Abstract base class of all character string scalar types. |
| `chararray` | Provides a convenient view on arrays of string and unicode values. |
| `choose` | Construct an array from an index array and a list of arrays to choose from. |
| `clip` | Given an interval, values outside the interval are clipped to the interval edges. |
| `clongdouble` | Complex number type composed of two extended-precision floating-point numbers. |
| `clongfloat` | Complex number type composed of two extended-precision floating-point numbers. |
| `column_stack` | Stack 1-D arrays as columns into a 2-D array. |
| `common_type` | Return a scalar type which is common to the input arrays. |
| `compare_chararrays` | Performs element-wise comparison of two string arrays using the comparison operator specified by 'cmp_op'. |
| `compat` | Compatibility module. |
| `complex128` | Complex number type composed of two double-precision floating-point numbers, compatible with Python 'complex'. |
| `complex256` | Complex number type composed of two extended-precision floating-point numbers. |
| `complex64` | Complex number type composed of two single-precision floating-point numbers. |
| `complex_` | Complex number type composed of two double-precision floating-point numbers, compatible with Python 'complex'. |
| `complexfloating` | Abstract base class of all complex number scalar types that are made up of floating-point numbers. |
| `compress` | Return selected slices of an array along given axis. |
| `concatenate` | Join a sequence of arrays along an existing axis. |
| `conj` | Return the complex conjugate, element-wise. |
| `conjugate` | Return the complex conjugate, element-wise. |
| `convolve` | Returns the discrete, linear convolution of two one-dimensional sequences. |
| `copy` | Return an array copy of the given object. |
| `copysign` | Change the sign of x1 to that of x2, element-wise. |
| `copyto` | Copies values from one array to another, broadcasting as necessary. |
| `core` | Contains the core of NumPy: ndarray, ufuncs, dtypes, etc. |
| `corrcoef` | Return Pearson product-moment correlation coefficients. |
| `correlate` | Cross-correlation of two 1-dimensional sequences. |
| `cos` | Cosine element-wise. |
| `cosh` | Hyperbolic cosine, element-wise. |
| `count_nonzero` | Counts the number of non-zero values in the array 'a'. |
| `cov` | Estimate a covariance matrix, given data and weights. |
| `cross` | Return the cross product of two (arrays of) vectors. |
| `csingle` | Complex number type composed of two single-precision floating-point numbers. |
| `cumprod` | Return the cumulative product of elements along a given axis. |
| `cumproduct` | Return the cumulative product over the given axis. |
| `cumsum` | Return the cumulative sum of the elements along a given axis. |
| `datetime64` | If created from a 64-bit integer, it represents an offset from '1970-01-01T00:00:00'. |
| `datetime_as_string` | Convert an array of datetimes into an array of strings. |
| `datetime_data` | Get information about the step size of a date or time type. |
| `deg2rad` | Convert angles from degrees to radians. |
| `degrees` | Convert angles from radians to degrees. |
| `delete` | Return a new array with sub-arrays along an axis deleted. |
| `deprecate` | Issues a Deprecation Warning, adds warning to 'old_name's docstring, rebinds 'old_name.\_\_name\_\_' and returns the new function object. |
| `deprecate_with_doc` | Deprecates a function and includes the deprecation in its docstring. |
| `diag` | Extract a diagonal or construct a diagonal array. |
| `diag_indices` | Return the indices to access the main diagonal of an array. |
| `diag_indices_from` | Return the indices to access the main diagonal of an n-dimensional array. |
| `diagflat` | Create a two-dimensional array with the flattened input as a diagonal. |
| `diagonal` | Return specified diagonals. |
| `diff` | Calculate the n-th discrete difference along the given axis. |
| `digitize` | Return the indices of the bins to which each value in input array belongs. |
| `disp` | Display a message on a device. |
| `divide` | Divide arguments element-wise. |
| `divmod` | Return element-wise quotient and remainder simultaneously. |
| `dot` | Dot product of two arrays. Specifically, |
| `double` | Double-precision floating-point number type, compatible with Python 'float' and C 'double'. |
| `dsplit` | Split array into multiple sub-arrays along the 3rd axis (depth). |
| `dstack` | Stack arrays in sequence depth wise (along third axis). |
| `e` | Convert a string or number to a floating point number, if possible. |
| `ediff1d` | The differences between consecutive elements of an array. |
| `einsum` | Evaluates the Einstein summation convention on the operands. |
| `einsum_path` | Evaluates the lowest cost contraction order for an einsum expression by considering the creation of intermediate arrays. |
| `emath` | Wrapper functions to more user-friendly calling of certain math functions. |
| `empty` | Return a new array of given shape and type, without initializing entries. |
| `empty_like` | Return a new array with the same shape and type as a given array. |
| `equal` | Return (x1 == x2) element-wise. |
| `errstate` | Context manager for floating-point error handling. |
| `euler_gamma` | Convert a string or number to a floating point number, if possible. |
| `exp` | Calculate the exponential of all elements in the input array. |
| `exp2` | Calculate '2^p' for all 'p' in the input array. |
| `expand_dims` | Insert a new axis that will appear at the 'axis' position in the expanded array shape. |
| `expm1` | Calculate 'exp(x) — 1' for all elements in the array. |
| `extract` | Return the elements of an array that satisfy some condition. |
| `eye` | Return a 2-D array with ones on the diagonal and zeros elsewhere. |
| `fabs` | Compute the absolute values element-wise. |
| `fill_diagonal` | Fill the main diagonal of the given array of any dimensionality. |
| `find_common_type` | Determine common type following standard coercion rules. |
| `finfo` | Machine limits for floating point types. |
| `fix` | Round to nearest integer towards zero. |
| `flatiter` | Flat iterator object to iterate over arrays. |
| `flatnonzero` | Return indices that are non-zero in the flattened version of a. |
| `flexible` | Abstract base class of all scalar types without predefined length. |
| `flip` | Reverse the order of elements in an array along the given axis. |
| `fliplr` | Reverse the order of elements along axis 1 (left/right). |
| `flipud` | Reverse the order of elements along axis 0 (up/down). |
| `float128` | Extended-precision floating-point number type, compatible with C 'long double' but not necessarily with IEEE 754 quadruple-precision. |
| `float16` | Half-precision floating-point number type. |
| `float32` | Single-precision floating-point number type, compatible with C 'float'. |
| `float64` | Double-precision floating-point number type, compatible with Python 'float' and C 'double'. |
| `float_` | Double-precision floating-point number type, compatible with Python 'float' and C 'double'. |
| `float_power` | First array elements raised to powers from second array, element-wise. |
| `floating` | Abstract base class of all floating-point scalar types. |
| `floor` | Return the floor of the input, element-wise. |
| `floor_divide` | Return the largest integer smaller or equal to the division of the inputs. |
| `fmax` | Element-wise maximum of array elements. |
| `fmin` | Element-wise minimum of array elements. |
| `fmod` | Returns the element-wise remainder of division. |
| `format_float_positional` | Format a floating-point scalar as a decimal string in positional notation. |
| `format_float_scientific` | Format a floating-point scalar as a decimal string in scientific notation. |
| `format_parser` | Class to convert formats, names, titles description to a dtype. |
| `frexp` | Decompose the elements of x into mantissa and twos exponent. |
| `frombuffer` | Interpret a buffer as a 1-dimensional array. |
| `fromfile` | Construct an array from data in a text or binary file. |
| `fromfunction` | Construct an array by executing a function over each coordinate. |
| `fromiter` | Create a new 1-dimensional array from an iterable object. |
| `frompyfunc` | Takes an arbitrary Python function and returns a NumPy ufunc. |
| `fromregex` | Construct an array from a text file, using regular expression parsing. |
| `fromstring` | A new 1-D array initialized from text data in a string. |
| `full` | Return a new array of given shape and type, filled with 'fill_value'. |
| `full_like` | Return a full array with the same shape and type as a given array. |

### `numpy` *G — O*

| Member | Description |
|:-------|:--------------|
| `gcd` | Returns the greatest common divisor of 'x1' and 'x2' |
| `generic` | Base class for numpy scalar types. |
| `genfromtxt` | Load data from a text file, with missing values handled as specified. |
| `geomspace` | Return numbers spaced evenly on a log scale (a geometric progression). |
| `get_array_wrap` | Find the wrapper for the array with the highest priority. |
| `get_include` | Return the directory that contains the NumPy \*.h header files. |
| `get_printoptions` | Return the current print options. |
| `getbufsize` | Return the size of the buffer used in ufuncs. |
| `geterr` | Get the current way of handling floating-point errors. |
| `geterrcall` | Return the current callback function used on floating-point errors. |
| `geterrobj` | Return the current object that defines floating-point error handling. |
| `gradient` | Return the gradient of an N-dimensional array. |
| `greater` | Return the truth value of `(x1 > x2)` element-wise. |
| `greater_equal` | Return the truth value of `(x1 >= x2)` element-wise. |
| `half` | Half-precision floating-point number type. |
| `hamming` | Return the Hamming window. |
| `hanning` | Return the Hanning window. |
| `heaviside` | Compute the Heaviside step function. |
| `histogram` | Compute the histogram of a dataset. |
| `histogram2d` | Compute the bi-dimensional histogram of two data samples. |
| `histogram_bin_edges` | Function to calculate only the edges of the bins used by the 'histogram' function. |
| `histogramdd` | Compute the multidimensional histogram of some data. |
| `hsplit` | Split an array into multiple sub-arrays horizontally (column-wise). |
| `hstack` | Stack arrays in sequence horizontally (column wise). |
| `hypot` | Given the "legs" of a right triangle, return its hypotenuse. |
| `i0` | Modified Bessel function of the first kind, order 0. |
| `identity` | Return the identity array. |
| `iinfo` | Machine limits for integer types. |
| `imag` | Return the imaginary part of the complex argument. |
| `in1d` | Test whether each element of a 1-D array is also present in a second array. |
| `index_exp` | A nicer way to build up index tuples for arrays. |
| `indices` | Return an array representing the indices of a grid. |
| `inexact` | Abstract base class of all numeric scalar types with a (potentially) inexact representation of the values in its range. |
| `inf` | Convert a string or number to a floating point number, if possible. |
| `info` | Get help information for an array, function, class, or module. |
| `infty` | Convert a string or number to a floating point number, if possible. |
| `inner` | Inner product of two arrays. |
| `insert` | Insert values along the given axis before the given indices. |
| `int0` | Signed integer type, compatible with Python 'int' and C 'long'. |
| `int16` | Signed integer type, compatible with C 'short'. |
| `int32` | Signed integer type, compatible with C 'int'. |
| `int64` | Signed integer type, compatible with Python 'int' and C 'long'. |
| `int8` | Signed integer type, compatible with C 'char'. |
| `int_` | Signed integer type, compatible with Python 'int' and C 'long'. |
| `intc` | Signed integer type, compatible with C 'int'. |
| `integer` | Abstract base class of all integer scalar types. |
| `interp` | One-dimensional linear interpolation for monotonically increasing sample points. |
| `intersect1d` | Find the intersection of two arrays. |
| `intp` | Signed integer type, compatible with Python 'int' and C 'long'. |
| `invert` | Compute bit-wise inversion, or bit-wise NOT, element-wise. |
| `is_busday` | Calculates which of the given dates are valid days, and which are not. |
| `isclose` | Returns a boolean array where two arrays are element-wise equal within a tolerance. |
| `iscomplex` | Returns a bool array, where True if input element is complex. |
| `iscomplexobj` | Check for a complex type or an array of complex numbers. |
| `isfinite` | Test element-wise for finiteness (not infinity and not Not a Number). |
| `isfortran` | Check if the array is Fortran contiguous but *not* C contiguous. |
| `isin` | Calculates 'element in test_elements', broadcasting over 'element' only. |
| `isinf` | Test element-wise for positive or negative infinity. |
| `isnan` | Test element-wise for NaN and return result as a boolean array. |
| `isnat` | Test element-wise for NaT (not a time) and return result as a boolean array. |
| `isneginf` | Test element-wise for negative infinity, return result as bool array. |
| `isposinf` | Test element-wise for positive infinity, return result as bool array. |
| `isreal` | Returns a bool array, where True if input element is real. |
| `isrealobj` | Return True if x is a not complex type or an array of complex numbers. |
| `isscalar` | Returns True if the type of 'element' is a scalar type. |
| `issctype` | Determines whether the given object represents a scalar data-type. |
| `issubclass_` | Determine if a class is a subclass of a second class. |
| `issubdtype` | Returns True if first argument is a typecode lower/equal in type hierarchy. |
| `issubsctype` | Determine if the first argument is a subclass of the second argument. |
| `iterable` | Check whether or not an object can be iterated over. |
| `ix_` | Construct an open mesh from multiple sequences. |
| `kaiser` | Return the Kaiser window. |
| `kernel_version` | Built-in immutable sequence. |
| `kron` | Kronecker product of two arrays. |
| `lcm` | Returns the lowest common multiple of 'x1' and 'x2' |
| `ldexp` | Returns x1 * 2^x2, element-wise. |
| `left_shift` | Shift the bits of an integer to the left. |
| `less` | Return the truth value of `(x1 < x2)` element-wise. |
| `less_equal` | Return the truth value of `(x1 <= x2)` element-wise. |
| `lexsort` | Perform an indirect stable sort using a sequence of keys. |
| `lib` | Note: almost all functions in the 'numpy.lib' namespace |
| `linspace` | Return evenly spaced numbers over a specified interval. |
| `little_endian` | `bool(x) -> bool` |
| `load` | Load arrays or pickled objects from '.npy', '.npz' or pickled files. |
| `loadtxt` | Load data from a text file. |
| `log` | Natural logarithm, element-wise. |
| `log10` | Return the base 10 logarithm of the input array, element-wise. |
| `log1p` | Return the natural logarithm of one plus the input array, element-wise. |
| `log2` | Base-2 logarithm of 'x'. |
| `logaddexp` | Logarithm of the sum of exponentiations of the inputs. |
| `logaddexp2` | Logarithm of the sum of exponentiations of the inputs in base-2. |
| `logical_and` | Compute the truth value of x1 AND x2 element-wise. |
| `logical_not` | Compute the truth value of NOT x element-wise. |
| `logical_or` | Compute the truth value of x1 OR x2 element-wise. |
| `logical_xor` | Compute the truth value of x1 XOR x2, element-wise. |
| `logspace` | Return numbers spaced evenly on a log scale. |
| `longcomplex` | Complex number type composed of two extended-precision floating-point numbers. |
| `longdouble` | Extended-precision floating-point number type, compatible with C 'long double' but not necessarily with IEEE 754 quadruple-precision. |
| `longfloat` | Extended-precision floating-point number type, compatible with C 'long double' but not necessarily with IEEE 754 quadruple-precision. |
| `longlong` | Signed integer type, compatible with C 'long long'. |
| `lookfor` | Do a keyword search on docstrings. |
| `mask_indices` | Return the indices to access (n, n) arrays, given a masking function. |
| `mat` | Interpret the input as a matrix. |
| `math` | This module provides access to the mathematical functions defined by the C standard. |
| `matmul` | Matrix product of two arrays. |
| `max` | Return the maximum of an array or maximum along an axis. |
| `maximum` | Element-wise maximum of array elements. |
| `maximum_sctype` | Return the scalar type of highest precision of the same kind as the input. |
| `may_share_memory` | Determine if two arrays might share memory |
| `mean` | Compute the arithmetic mean along the specified axis. |
| `median` | Compute the median along the specified axis. |
| `memmap` | Create a memory-map to an array stored in a *binary* file on disk. |
| `meshgrid` | Return a list of coordinate matrices from coordinate vectors. |
| `mgrid` | An instance which returns a dense multi-dimensional "meshgrid". |
| `min` | Parameters |
| `min_scalar_type` | For scalar 'a', returns the data type with the smallest size and smallest scalar kind which can hold its value. |
| `minimum` | Element-wise minimum of array elements. |
| `mintypecode` | Return the character for the minimum-size type to which given types can be safely cast. |
| `mod` | Returns the element-wise remainder of division. |
| `modf` | Return the fractional and integral parts of an array, element-wise. |
| `moveaxis` | Other axes remain in their original order. |
| `msort` | Return a copy of an array sorted along the first axis. |
| `multiply` | Multiply arguments element-wise. |
| `nan` | Convert a string or number to a floating point number, if possible. |
| `nan_to_num` | Replace NaN with zero and infinity with large finite numbers (default behaviour). |
| `nanargmax` | Return the indices of the maximum values in the specified axis ignoring NaNs. |
| `nanargmin` | Return the indices of the minimum values in the specified axis ignoring NaNs. |
| `nancumprod` | Return the cumulative product of array elements over a given axis treating Not a Numbers (NaNs) as one. |
| `nancumsum` | Return the cumulative sum of array elements over a given axis treating Not a Numbers (NaNs) as zero. |
| `nanmax` | Return the maximum of an array or maximum along an axis, ignoring any NaNs. |
| `nanmean` | Compute the arithmetic mean along the specified axis, ignoring NaNs. |
| `nanmedian` | Compute the median along the specified axis, while ignoring NaNs. |
| `nanmin` | Return minimum of an array or minimum along an axis, ignoring any NaNs. |
| `nanpercentile` | Compute the qth percentile of the data along the specified axis, while ignoring nan values. |
| `nanprod` | Return the product of array elements over a given axis treating Not a Numbers (NaNs) as ones. |
| `nanquantile` | Compute the qth quantile of the data along the specified axis, while ignoring nan values. |
| `nanstd` | Compute the standard deviation along the specified axis, while ignoring NaNs. |
| `nansum` | Return the sum of array elements over a given axis treating Not a Numbers (NaNs) as zero. |
| `nanvar` | Compute the variance along the specified axis, while ignoring NaNs. |
| `nbytes` | Base object for a dictionary for look-up with any alias for an array dtype. |
| `ndenumerate` | Multidimensional index iterator. |
| `ndim` | Return the number of dimensions of an array. |
| `ndindex` | An N-dimensional iterator object to index arrays. |
| `nditer` | Efficient multi-dimensional iterator object to iterate over arrays. |
| `negative` | Numerical negative, element-wise. |
| `nested_iters` | Create nditers for use in nested loops |
| `nextafter` | Return the next floating-point value after x1 towards x2, element-wise. |
| `nonzero` | Return the indices of the elements that are non-zero. |
| `not_equal` | Return (x1 != x2) element-wise. |
| `numarray` | Help for removed not found. |
| `number` | Abstract base class of all numeric scalar types. |
| `obj2sctype` | Return the scalar dtype or NumPy equivalent of Python type of an object. |
| `object0` | Any Python object. |
| `object_` | Any Python object. |
| `ogrid` | An instance which returns an open multi-dimensional "meshgrid". |
| `oldnumeric` | Help for removed not found. |
| `ones` | Return a new array of given shape and type, filled with ones. |
| `ones_like` | Return array of ones with the same shape and type as given array. |
| `outer` | Compute the outer product of two vectors. |

### `numpy` *P — Z*

| Member | Description |
|:-------|:--------------|
| `packbits` | Packs the elements of a binary-valued array into bits in a uint8 array. |
| `pad` | Pad an array. |
| `partition` | Return a partitioned copy of an array. |
| `percentile` | Compute the q-th percentile of the data along the specified axis. |
| `pi` | Convert a string or number to a floating point number, if possible. |
| `piecewise` | Evaluate a piecewise-defined function. |
| `place` | Change elements of an array based on conditional and input values. |
| `poly` | Find the coefficients of a polynomial with the given sequence of roots. |
| `poly1d` | A one-dimensional polynomial class. |
| `polyadd` | Find the sum of two polynomials. |
| `polyder` | Return the derivative of the specified order of a polynomial. |
| `polydiv` | Returns the quotient and remainder of polynomial division. |
| `polyfit` | Least squares polynomial fit. |
| `polyint` | Return an antiderivative (indefinite integral) of a polynomial. |
| `polymul` | Find the product of two polynomials. |
| `polynomial` | A sub-package for efficiently dealing with polynomials. |
| `polysub` | Difference (subtraction) of two polynomials. |
| `polyval` | Evaluate a polynomial at specific values. |
| `positive` | Numerical positive, element-wise. |
| `power` | First array elements raised to powers from second array, element-wise. |
| `printoptions` | Context manager for setting print options. |
| `prod` | Return the product of array elements over a given axis. |
| `product` | Return the product of array elements over a given axis. |
| `promote_types` | Returns the data type with the smallest size and smallest scalar kind to which both 'type1' and 'type2' may be safely cast. |
| `ptp` | Range of values (maximum — minimum) along an axis. |
| `put` | Replaces specified elements of an array with given values. |
| `put_along_axis` | Put values into the destination array by matching 1d index and data slices. |
| `putmask` | Changes elements of an array based on conditional and input values. |
| `quantile` | Compute the q-th quantile of the data along the specified axis. |
| `r_` | Translates slice objects to concatenation along the first axis. |
| `rad2deg` | Convert angles from radians to degrees. |
| `radians` | Convert angles from degrees to radians. |
| `ravel` | Return a contiguous flattened array. |
| `ravel_multi_index` | Converts a tuple of index arrays into an array of flat indices, applying boundary modes to the multi-index. |
| `real` | Return the real part of the complex argument. |
| `real_if_close` | If input is complex with all imaginary parts close to zero, return real parts. |
| `rec` | Record Arrays |
| `recarray` | Construct an ndarray that allows field access using attributes. |
| `recfromcsv` | Load ASCII data stored in a comma-separated file. |
| `recfromtxt` | Load ASCII data from a file and return it in a record array. |
| `reciprocal` | Return the reciprocal of the argument, element-wise. |
| `record` | A data-type scalar that allows field access as attribute lookup. |
| `remainder` | Returns the element-wise remainder of division. |
| `repeat` | Repeat each element of an array after themselves |
| `require` | Return an ndarray of the provided type that satisfies requirements. |
| `reshape` | Gives a new shape to an array without changing its data. |
| `resize` | Return a new array with the specified shape. |
| `result_type` | Returns the type that results from applying the NumPy |
| `right_shift` | Shift the bits of an integer to the right. |
| `rint` | Round elements of the array to the nearest integer. |
| `roll` | Roll array elements along a given axis. |
| `rollaxis` | Roll the specified axis backwards, until it lies in a given position. |
| `roots` | Return the roots of a polynomial with coefficients given in p. |
| `rot90` | Rotate an array by 90 degrees in the plane specified by axes. |
| `round` | Evenly round to the given number of decimals. |
| `round_` | Round an array to the given number of decimals. |
| `row_stack` | Stack arrays in sequence vertically (row wise). |
| `s_` | A nicer way to build up index tuples for arrays. |
| `safe_eval` | Protected string evaluation. |
| `save` | Save an array to a binary file in NumPy '.npy' format. |
| `savetxt` | Save an array to a text file. |
| `savez` | Save several arrays into a single file in uncompressed '.npz' format. |
| `savez_compressed` | Save several arrays into a single file in compressed '.npz' format. |
| `sctype2char` | Return the string representation of a scalar dtype. |
| `sctypeDict` | dict() `->` new empty dictionary |
| `sctypes` | dict() `->` new empty dictionary |
| `searchsorted` | Find indices where elements should be inserted to maintain order. |
| `select` | Return an array drawn from elements in choicelist, depending on conditions. |
| `set_numeric_ops` | Set numerical operators for array objects. |
| `set_printoptions` | Set printing options. |
| `set_string_function` | Set a Python function to be used when pretty printing arrays. |
| `setbufsize` | Set the size of the buffer used in ufuncs. |
| `setdiff1d` | Find the set difference of two arrays. |
| `seterr` | Set how floating-point errors are handled. |
| `seterrcall` | Set the floating-point error callback function or log object. |
| `seterrobj` | Set the object that defines floating-point error handling. |
| `setxor1d` | Find the set exclusive-or of two arrays. |
| `shape` | Return the shape of an array. |
| `shares_memory` | Determine if two arrays share memory. |
| `short` | Signed integer type, compatible with C 'short'. |
| `show_config` | Show libraries and system information on which NumPy was built and is being used |
| `sign` | Returns an element-wise indication of the sign of a number. |
| `signbit` | Returns element-wise True where signbit is set (less than zero). |
| `signedinteger` | Abstract base class of all signed integer scalar types. |
| `sin` | Trigonometric sine, element-wise. |
| `sinc` | Return the normalized sinc function. |
| `single` | Single-precision floating-point number type, compatible with C 'float'. |
| `singlecomplex` | Complex number type composed of two single-precision floating-point |
| `sinh` | Hyperbolic sine, element-wise. |
| `size` | Return the number of elements along a given axis. |
| `sometrue` | Check whether some values are true. |
| `sort` | Return a sorted copy of an array. |
| `sort_complex` | Sort a complex array using the real part first, then the imaginary part. |
| `source` | Print or write to a file the source code for a NumPy object. |
| `spacing` | Return the distance between x and the nearest adjacent number. |
| `split` | Split an array into multiple sub-arrays as views into 'ary'. |
| `sqrt` | Return the non-negative square-root of an array, element-wise. |
| `square` | Return the element-wise square of the input. |
| `squeeze` | Remove axes of length one from 'a'. |
| `stack` | Join a sequence of arrays along a new axis. |
| `std` | Compute the standard deviation along the specified axis. |
| `str0` | A unicode string. |
| `str_` | A unicode string. |
| `string_` | A byte string. |
| `subtract` | Subtract arguments, element-wise. |
| `sum` | Sum of array elements over a given axis. |
| `swapaxes` | Interchange two axes of an array. |
| `take` | Take elements from an array along an axis. |
| `take_along_axis` | Take values from the input array by matching 1d index and data slices. |
| `tan` | Compute tangent element-wise. |
| `tanh` | Compute hyperbolic tangent element-wise. |
| `tensordot` | Compute tensor dot product along specified axes. |
| `tile` | Construct an array by repeating A the number of times given by reps. |
| `timedelta64` | A timedelta stored as a 64-bit integer. |
| `trace` | Return the sum along diagonals of the array. |
| `tracemalloc_domain` | Convert a number or string to an integer, or return 0 if no arguments are given. |
| `transpose` | Returns an array with axes transposed. |
| `trapz` | Integrate along the given axis using the composite trapezoidal rule. |
| `tri` | An array with ones at and below the given diagonal and zeros elsewhere. |
| `tril` | Lower triangle of an array. |
| `tril_indices` | Return the indices for the lower-triangle of an (n, m) array. |
| `tril_indices_from` | Return the indices for the lower-triangle of arr. |
| `trim_zeros` | Trim the leading and/or trailing zeros from a 1-D array or sequence. |
| `triu` | Upper triangle of an array. |
| `triu_indices` | Return the indices for the upper-triangle of an (n, m) array. |
| `triu_indices_from` | Return the indices for the upper-triangle of arr. |
| `true_divide` | Divide arguments element-wise. |
| `trunc` | Return the truncated value of the input, element-wise. |
| `typecodes` | dict() `->` new empty dictionary |
| `typename` | Return a description for the given data type code. |
| `ubyte` | Unsigned integer type, compatible with C 'unsigned char'. |
| `ufunc` | Functions that operate element by element on whole arrays. |
| `uint` | Unsigned integer type, compatible with C 'unsigned long'. |
| `uint0` | Unsigned integer type, compatible with C 'unsigned long'. |
| `uint16` | Unsigned integer type, compatible with C 'unsigned short'. |
| `uint32` | Unsigned integer type, compatible with C 'unsigned int'. |
| `uint64` | Unsigned integer type, compatible with C 'unsigned long'. |
| `uint8` | Unsigned integer type, compatible with C 'unsigned char'. |
| `uintc` | Unsigned integer type, compatible with C 'unsigned int'. |
| `uintp` | Unsigned integer type, compatible with C 'unsigned long'. |
| `ulonglong` | Signed integer type, compatible with C 'unsigned long long'. |
| `unicode_` | A unicode string. |
| `union1d` | Find the union of two arrays. |
| `unique` | Find the unique elements of an array. |
| `unpackbits` | Unpacks elements of a uint8 array into a binary-valued output array. |
| `unravel_index` | Converts a flat index or array of flat indices into a tuple of coordinate arrays. |
| `unsignedinteger` | Abstract base class of all unsigned integer scalar types. |
| `unwrap` | Unwrap by taking the complement of large deltas with respect to the period. |
| `ushort` | Unsigned integer type, compatible with C 'unsigned short'. |
| `vander` | Generate a Vandermonde matrix. |
| `var` | Compute the variance along the specified axis. |
| `vdot` | Return the dot product of two vectors. |
| `vectorize` | Returns an object that acts like pyfunc, but takes arrays as input. |
| `void` | Create a new structured or unstructured void scalar. |
| `void0` | Create a new structured or unstructured void scalar. |
| `vsplit` | Split an array into multiple sub-arrays vertically (row-wise). |
| `vstack` | Stack arrays in sequence vertically (row wise). |
| `where` | Return elements chosen from 'x' or 'y' depending on 'condition'. |
| `who` | Print the NumPy arrays in the given dictionary. |
| `zeros` | Return a new array of given shape and type, filled with zeros. |
| `zeros_like` | Return an array of zeros with the same shape and type as a given array. |

## C.2  `numpy.ndarray`

```
ndarray(shape, dtype=float, buffer=None, offset=0, 
        strides=None, order=None)

An array object represents a multidimensional, homogeneous 
array of fixed-size items. An associated data-type object 
describes the format of each element in the array (its 
byte-order, how many bytes it occupies in memory, whether 
it is an integer, a floating point number, or something 
else, etc.)

Arrays should be constructed using `array`, `zeros` or 
`empty`. The parameters given here refer to a low-level 
method (`ndarray(...)`) for instantiating an array.

Parameters
----------
shape : tuple of ints
    Shape of created array.
dtype : data-type, optional
    Any object that can be interpreted as numpy data type.
buffer : object exposing buffer interface, optional
    Used to fill the array with data.
offset : int, optional
    Offset of array data in buffer.
strides : tuple of ints, optional
    Strides of data in memory.
order : {'C', 'F'}, optional
    Row-major (C-style) or column-major order.
```

| Member | Description |
|:-------|:--------------|
| `alignment` | The required alignment (bytes) of this data-type according to the compiler. |
| `base` | Returns dtype for the base element of the subarrays, regardless of their dimension or shape. |
| `byteorder` | Character indicating the byte-order of this dtype object. |
| `char` | A unique character code for each of the built-in types. |
| `descr` | '\_\_array_interface\_\_' description of the data-type. |
| `fields` | Dictionary of named fields defined for this type or None. |
| `flags` | Bit-flags describing how this data type is to be interpreted. |
| `hasobject` | Boolean indicating whether this dtype contains any reference-counted objects in any fields or sub-dtypes. |
| `isalignedstruct` | Boolean indicating whether the dtype is a struct which maintains field alignment. |
| `isbuiltin` | Integer indicating how this dtype relates to built-in dtypes. |
| `isnative` | Boolean indicating whether the byte order of this dtype is native to the platform. |
| `itemsize` | The element size of this data-type object. |
| `kind` | A character code (one of 'biufcmMOSUV') identifying the general kind of data. |
| `metadata` | None, or readonly dict of metadata (mappingproxy). |
| `name` | A bit-width name for this data-type. |
| `names` | Ordered list of field names, or 'None' if there are no fields. |
| `ndim` | Number of dimensions of the sub-array if this data type describes a sub-array, and '0' otherwise. |
| `newbyteorder` | Return a new dtype with a different byte order. |
| `num` | A unique number for each of the 21 different built-in types. |
| `shape` | Shape tuple of the sub-array if this data type describes a sub-array, and '()' otherwise. |
| `str` | The array-protocol typestring of this data-type object. |
| `subdtype` | Tuple '(item_dtype, shape)' if this 'dtype' describes a sub-array, and None otherwise. |

## C.3  `numpy.dtype`

```
dtype(dtype, align=False, copy=False, [metadata])

Create a data type object. 

A numpy array is homogeneous, and contains elements 
described by a dtype object. A dtype object can be 
constructed from different combinations of fundamental 
numeric types.

Parameters
----------
dtype
    Object to be converted to a data type object.
align : bool, optional
    Add padding to the fields to match what a C compiler 
    would output for a similar C-struct. Can be 'True' 
    only if `obj` is a dictionary or a comma-separated 
    string. If a struct dtype is being created, this also 
    sets a sticky alignment flag 'isalignedstruct'.
copy : bool, optional
    Make a new copy of the data-type object. If 'False', 
    the result may just be a reference to a built-in 
    data-type object.
metadata : dict, optional
    An optional dictionary with dtype metadata.
```

| Member | Description |
|:-------|:--------------|
| `alignment` | The required alignment (bytes) of this data-type according to the compiler. |
| `base` | Returns dtype for the base element of the subarrays, regardless of their dimension or shape. |
| `byteorder` | A character indicating the byte-order of this data-type object. |
| `char` | A unique character code for each of the 21 different built-in types. |
| `descr` | '\_\_array_interface\_\_' description of the data-type. |
| `fields` | Dictionary of named fields for this type or None. |
| `flags` | Bit-flags describing how this data type is to be interpreted. |
| `hasobject` | Boolean indicating whether this dtype contains any reference-counted objects in any fields or sub-dtypes. |
| `isalignedstruct` | Boolean indicating whether the dtype is a struct which maintains field alignment. |
| `isbuiltin` | Integer indicating how this dtype relates to the built-in dtypes. |
| `isnative` | Boolean indicating whether the byte order of this dtype is native to the platform. |
| `itemsize` | The element size of this data-type object. |
| `kind` | A character code (one of 'biufcmMOSUV') identifying the general kind of data. |
| `metadata` | Either 'None' or a readonly dictionary of metadata. |
| `name` | A bit-width name for this data-type. |
| `names` | Ordered list of field names, or 'None' if there are no fields. |
| `ndim` | Number of dimensions of the sub-array if this data type describes a sub-array, and '0' otherwise. |
| `newbyteorder` | Return a new dtype with a different byte order. |
| `num` | A unique number for each of the 21 different built-in types. |
| `shape` | Shape tuple of the sub-array if this data type describes a sub-array, and '()' otherwise. |
| `str` | The array-protocol typestring of this data-type object. |
| `subdtype` | Tuple '(item_dtype, shape)' if this 'dtype' describes a sub-array, and None otherwise. |

## C.4  `numpy.linalg`
 
```
The NumPy linear algebra functions rely on BLAS and 
LAPACK to provide efficient low level implementations 
of standard linear algebra algorithms. Those libraries 
may be provided by NumPy itself using C versions of a 
subset of their reference implementations but, when 
possible, highly optimized libraries that take 
advantage of specialized processor functionality are 
preferred. Examples of such libraries are OpenBLAS, 
MKL (TM), and ATLAS. Because those libraries are 
multithreaded and processor dependent, environmental 
variables and external packages such as threadpoolctl 
may be needed to control the number of threads or 
specify the processor architecture. 

- OpenBLAS: https://www.openblas.net/
- threadpoolctl: https://github.com/joblib/threadpoolctl

Please note that the most-used linear algebra functions 
in NumPy are present in the main 'numpy' namespace 
rather than in 'numpy.linalg'. There are: 'dot', 
'vdot', 'inner', 'outer', 'matmul', 'tensordot', 
'einsum', 'einsum_path' and 'kron'.
```

| Member | Description |
|:-------|:--------------|
| `cholesky` | Cholesky decomposition. |
| `cond` | Compute the condition number of a matrix. |
| `det` | Compute the determinant of an array. |
| `eig` | Compute eigenvalues & right eigenvectors of a square array. |
| `eigh` | Return the eigenvalues and eigenvectors of a complex Hermitian (conjugate symmetric) or a real symmetric matrix. |
| `eigvals` | Compute the eigenvalues of a general matrix. |
| `eigvalsh` | Compute the eigenvalues of a complex Hermitian or real symmetric matrix. |
| `inv` | Compute the (multiplicative) inverse of a matrix. |
| `lstsq` | Return the least-squares solution to a linear matrix equation. |
| `matrix_power` | Raise a square matrix to the (integer) power 'n'. |
| `matrix_rank` | Return matrix rank of array using SVD method |
| `multi_dot` | Compute the dot product of two or more arrays in a single function call, while automatically selecting the fastest evaluation order. |
| `norm` | Matrix or vector norm. |
| `pinv` | Compute the (Moore-Penrose) pseudo-inverse of a matrix. |
| `qr` | Compute the qr factorization of a matrix. |
| `slogdet` | Compute the sign and (natural) logarithm of the determinant of an array. |
| `solve` | Solve a linear matrix equation, or system of linear scalar equations. |
| `svd` | Singular Value Decomposition. |
| `tensorinv` | Compute the 'inverse' of an N-dimensional array. |
| `tensorsolve` | Solve the tensor equation 'a x = b' for x. |

## C.5  `numpy.fft`

```
The SciPy module 'scipy.fft' is a more comprehensive 
superset of 'numpy.fft', which includes only a basic 
set of routines. 
```

| Member | Description |
|:-------|:--------------|
| `fft` | Compute the one-dimensional discrete Fourier Transform. |
| `fft2` | Compute the 2-dimensional discrete Fourier Transform. |
| `fftfreq` | Return the Discrete Fourier Transform sample frequencies. |
| `fftn` | Compute the N-dimensional discrete Fourier Transform. |
| `fftshift` | Shift the zero-frequency component to the center of the spectrum. |
| `helper` | Discrete Fourier Transforms — helper.py |
| `hfft` | Compute the FFT of a signal that has Hermitian symmetry, i.e., a real spectrum. |
| `ifft` | Compute the one-dimensional inverse discrete Fourier Transform. |
| `ifft2` | Compute the 2-dimensional inverse discrete Fourier Transform. |
| `ifftn` | Compute the N-dimensional inverse discrete Fourier Transform. |
| `ifftshift` | The inverse of 'fftshift'. Although identical for even-length 'x', the functions differ by one sample for odd-length 'x'. |
| `ihfft` | Compute the inverse FFT of a signal that has Hermitian symmetry. |
| `irfft` | Computes the inverse of 'rfft'. |
| `irfft2` | Computes the inverse of 'rfft2'. |
| `irfftn` | Computes the inverse of 'rfftn'. |
| `rfft` | Compute the one-dimensional discrete Fourier Transform for real input. |
| `rfft2` | Compute the 2-dimensional FFT of a real array. |
| `rfftfreq` | Return the Discrete Fourier Transform sample frequencies (for usage with rfft, irfft). |
| `rfftn` | Compute the N-dimensional discrete Fourier Transform for real input. |

## C.6  `numpy.random`

The `numpy.random` module is a NumPy sub-package, primarily used for generating random numbers and performing various statistical operations. The module provides a suite of functions that support many aspects of randomisation and probability distributions.

| Member | Description |
|:-------|:--------------|
| `beta` | Draw samples from a Beta distribution. |
| `binomial` | Draw samples from a binomial distribution. |
| `bit_generator` | BitGenerator base class and SeedSequence used to seed the BitGenerators. |
| `bytes` | Return random bytes. |
| `chisquare` | Draw samples from a chi-square distribution. |
| `choice` | Generates a random sample from a given 1-D array |
| `default_rng` | Construct a new Generator with the default BitGenerator (PCG64). |
| `dirichlet` | Draw samples from the Dirichlet distribution. |
| `exponential` | Draw samples from an exponential distribution. |
| `f` | Draw samples from an F distribution. |
| `gamma` | Draw samples from a Gamma distribution. |
| `geometric` | Draw samples from the geometric distribution. |
| `get_state` | Return a tuple representing the internal state of the generator. |
| `gumbel` | Draw samples from a Gumbel distribution. |
| `hypergeometric` | Draw samples from a Hypergeometric distribution. |
| `laplace` | Draw samples from the Laplace or double exponential distribution with specified location (or mean) and scale (decay). |
| `logistic` | Draw samples from a logistic distribution. |
| `lognormal` | Draw samples from a log-normal distribution. |
| `logseries` | Draw samples from a logarithmic series distribution. |
| `multinomial` | Draw samples from a multinomial distribution. |
| `multivariate_normal` | Draw random samples from a multivariate normal distribution. |
| `negative_binomial` | Draw samples from a negative binomial distribution. |
| `noncentral_chisquare` | Draw samples from a noncentral chi-square distribution. |
| `noncentral_f` | Draw samples from the noncentral F distribution. |
| `normal` | Draw random samples from a normal (Gaussian) distribution. |
| `pareto` | Draw samples from a Pareto II or Lomax distribution with specified shape. |
| `permutation` | Randomly permute a sequence, or return a permuted range. |
| `poisson` | Draw samples from a Poisson distribution. |
| `power` | Draws samples in [0, 1] from a power distribution with positive exponent a — 1. |
| `rand` | Random values in a given shape. |
| `randint` | Return random integers from 'low' (inclusive) to 'high' (exclusive). |
| `randn` | Return a sample (or samples) from the "standard normal" distribution. |
| `random` | Return random floats in the half-open interval [0.0, 1.0). |
| `random_integers` | Random integers of type 'np.int_' between 'low' and 'high', inclusive. |
| `random_sample` | Return random floats in the half-open interval [0.0, 1.0). |
| `ranf` | This is an alias of 'random_sample'. See 'random_sample'  for the complete documentation. |
| `rayleigh` | Draw samples from a Rayleigh distribution. |
| `sample` | This is an alias of 'random_sample'. See 'random_sample'  for the complete documentation. |
| `seed` | Reseed the singleton RandomState instance. |
| `set_state` | Set the internal state of the generator from a tuple. |
| `shuffle` | Modify a sequence in-place by shuffling its contents. |
| `standard_cauchy` | Draw samples from a standard Cauchy distribution with mode = 0. |
| `standard_exponential` | Draw samples from the standard exponential distribution. |
| `standard_gamma` | Draw samples from a standard Gamma distribution. |
| `standard_normal` | Draw samples from a standard Normal distribution (mean=0, stdev=1). |
| `standard_t` | Draw samples from a standard Student's t distribution with 'df' degrees of freedom. |
| `triangular` | Draw samples from the triangular distribution over the interval '[left, right]'. |
| `uniform` | Draw samples from a uniform distribution. |
| `vonmises` | Draw samples from a von Mises distribution. |
| `wald` | Draw samples from a Wald, or inverse Gaussian, distribution. |
| `weibull` | Draw samples from a Weibull distribution. |
| `zipf` | Draw samples from a Zipf distribution. |

[^numpyclassref]: NumPy docs. numpy.org. `https://numpy.org/doc/stable/reference/arrays.classes.html`
