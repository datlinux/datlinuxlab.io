> [🛒️ Get this book as a PDF or EPUB e-book from Leanpub](https://leanpub.com/numpybyexample).

# Chapter 5. Array Inspection

Inspecting a NumPy array involves examining its properties and attributes to gain a better understanding of its characteristics and contents. The following examples rely on the array defined here: 

```python
a = np.array([(1., 2.), (3., 4.)]) 
print(a)
```

```
[[1. 2.]
 [3. 4.]]
```

## 5.1  Shape & Size Information

#### Example 5-1  

Get the shape of the array — a tuple indicating the length of each dimension: 

```python
a.shape
```

```
(2, 2)
```

```{note}
In "regular" Python scripting you would need to use `print()` to output an expression. However, in an IPython (including Jupyter) environment there's no need to use `print()` explicitly (although you can) to see output — expressions are automatically evaluated and output to the console or adjacent to the Jupyter cell. 
```

#### Example 5-2  

The number of dimensions: 

```python
a.ndim
```

```
2
```

#### Example 5-3  

The total number of elements in the array: 

```python
a.size
```

```
4
```

#### Example 5-4  

Length of an element, in bytes: 

```python
# This is directly associated with the array's type..
a.itemsize
```

```
8
```

#### Example 5-5 

Information about the memory layout of the array: 

```python
a.flags
```

```
C_CONTIGUOUS : True
F_CONTIGUOUS : False
OWNDATA : True
WRITEABLE : True
ALIGNED : True
WRITEBACKIFCOPY : False
UPDATEIFCOPY : False
```

## 5.2  Truth Evaluation

An array value will evaluate to True if it represents anything other than zero.

#### Example 5-6  

Test if *all* elements in an array evaluate to True: 

```python
np.all(a)
```

```
True
```

#### Example 5-7  

Test if *any* (at least a single) element evaluates to True: 

```python
np.any(a)
```

```
True
```

## 5.3  Type Properties

#### Example 5-8  

The array's data type (`dtype`): 

```python
a.dtype  # OR: np.dtype(a)
```

```
dtype('float64')
```

#### Example 5-9  

Name of the array's data type: 

```python
a.dtype.name
```

```
'float64'
```

#### Example 5-10 

Character code of the data type: 

```python
a.dtype.char
```

```
'd'
```

#### Example 5-11  

Get the unique number for this data type: 

```python
a.dtype.num
```

```
12
```

## 5.4  String Representation

#### Example 5-12 

Get a printable string of an array's contents: 

```python
np.array2string(a)
```

```
'[[1. 2.]\n [3. 4.]]'
```

#### Example 5-13 

Get a string of an array plus info about its type: 

```python
a = np.array([(1, 2), (3, 4)], np.int32) 
np.array_repr(a)
```

```
'array([[1, 2],\n [3, 4]], dtype=int32)'
```

### See also:

| Function                | Description                                                          |
|-------------------------|----------------------------------------------------------------------|
| `numpy.dtype.byteorder` | A character indicating the byte-order of a `dtype` object            |
| `numpy.dtype.fields`    | Dictionary of names defined for this data type, or `None`            |
| `numpy.dtype.flags`     | Bit-flags describing how this data type is to be interpreted         |
| `numpy.dtype.isbuiltin` | Integer indicating how this dtype relates to the built-in dtypes     |
| `numpy.dtype.isnative`  | Boolean if the byte order of `dtype` is native to the platform       |
| `numpy.dtype.kind`      | A character code (one of ‘biufcmMOSUV’) identifying the kind of data |
| `numpy.ndarray.strides` | Tuple of bytes to step in each dimension when traversing an array    |
| `numpy.ndarray.nbytes`  | Total bytes consumed by the elements of the array                    |
| `numpy.nonzero`         | Return the indices of the elements that are non-zero                 |

## 5.5  Exercises 

These exercises refer to the following 2-D array: 

```python
a = np.array([(1, 2, 3), (4, 5, 6)]) 
```

### Exercise 5-1

What is the shape of the above NumPy array? Use the array's `shape` property to confirm your conclusion.

### Exercise 5-2

What is the size of each element in this array, in bytes?

### Exercise 5-3

Use the appropriate inspection property to find the total bytes consumed by the array. How does this compare to the multiple of the previous exercise's result by the total count of elements?

### Exercise 5-4

What is the string name of the data type of this array?

### Exercise 5-5

If a third row containing the elements `[7 8 9]` was added to the array, what would be the number of dimensions?
