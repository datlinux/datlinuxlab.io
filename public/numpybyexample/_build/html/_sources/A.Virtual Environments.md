> [🛒️ Get this book as a PDF or EPUB e-book from Leanpub](https://leanpub.com/numpybyexample).

# Appendix A. Virtual Environments

When you install a lot of software packages over time, your system can become bloated and can run the risk of programs failing. This can be due to conflicting package versions, or packages that were removed inadvertently that a program might rely on. Or maybe you've tried to run different versions of the same application (with potentially conflicting dependencies) for testing. 

A solution to address these problems is to create virtual environments – isolated, self-contained 'sand-boxes' that are perfect for protecting your applications from potential dependency and usage conflicts. 

## A.1  `virtualenv`

### Install `virtualenv`

One of the most popular programs for creating virtual environments is *virtualenv*, itself a package that you install via PyPI:  

```python
pip install virtualenv
```

Once installed you're ready to create virtual environments. It's a good idea to have a top-level folder under which all your virtual environments reside. From now on we'll refer to a virtual environment simply as a *"venv"*.

![](img/info.png)
Another benefit of *venv*s is reproducibility. You can create a sand-box with the exact software, packages, and package versions you require to reflect another environment, such as what you may have in production. This ensures every contributor to a project has an identical set-up. 

### Create and activate a virtual env't

To create a *venv*, change into the directory of the parent location for your *venvs*. Existing *venv*s will reside here as sub-directories. To create a *venv* run the following command: 

```python
python -m venv my-venv
```

The name of the *venv* can be anything you like, but of course uniquely named under this location. To enter a *venv* you need to 'activate' it, for example: 

```python
source my-venv/bin/activate
```

You can do this from anywhere by passing the fully qualified path to the *venv* folder:

```python
source /path/to/my-venv/bin/activate
```

In windows this might look like this (a `.` (dot) operator is equivalent to `source`):

```python
. C:\path\to\my-venv\bin\activate
```

When you enter the *venv*, you'll notice a special prompt that informs you that you're inside a *venv*. For example:

```
(my-venv) user@host:~/:$
```

Once you're in the *venv*, you can execute python scripts or install packages as you would normally. 

```python
python --version
```

```
'Python 3.10.12'
```

```python
pip install numpy jupyterlab
```

*(Output not shown)*

Packages will be installed for this *venv* without knowledge of other *venv*s. You can then run your programs as you would in a global environment: 

```python
jupyter lab  
# [Ctrl+c] to stop Jupyter..
```

### Exit the virtual env't

To exit out of a *venv*, shut down any running programs and type: 

```python
deactivate
```

and you'll be returned to a regular console prompt.

There's a lot more to virtual environments in Python (including the ability to enter/execute/exit them via shell scripts), so consult the online documentation or follow a good tutorial. 

## A.2  Docker Containers

Docker is a modern, small footprint alternative to hardware virtualisation[^docker]. Instead of running an entire guest operating system as a virtual machine (composed of the full complement of an operating system's disk, memory, and processing designated in advance), Docker containers are light-weight system shells that install the bare minimum that's required to run an application. 

A container then relies on the host's infrastructure for access to the operating system kernel and interfaces. This means you can run multiple Docker containers on commodity hardware. Containers are sand-boxed environments that isolate your apps, inside what looks like — to them — a stand alone operating system, of which there are many variants, most commonly Linux based. 

Docker containers can be created from scratch, but there are also images available that you can point to that are already set up with all or most of an application's needs. There's even a Jupyter image which you can use to launch a Jupyter-ready container after Docker is installed[^jupimage].

The easiest way to install and get Docker running is to use Docker Desktop[^dd]. Once you have it installed, start Docker Desktop, and this will also start the Docker engine. You can find and install images via the "Images — Hub" area in the Docker Desktop application, otherwise it's also very easy to do via a command line: 

```python
docker pull quay.io/jupyter/scipy-notebook
```

Once this completes, you can run Jupyter as follows:

```python
docker run -p 10000:8888 quay.io/jupyter/scipy-notebook
```

This will launch Jupyter in a web browser at the `localhost:8888/` URL, and you can start working with Python notebooks. 

```{note}
The Docker Desktop app also allows you to start and stop, or manage Docker containers that were installed either via the application interface or from the command line (it will detect them). It's a good idea to become familiar with Docker Desktop's features using the 'help' documentation. 
```

[^docker]: Docker. `https://www.docker.com`

[^jupimage]: Jupyter image. `https://jupyter-docker-stacks.readthedocs.io/en/latest`

[^dd]: Docker Desktop. `https://docs.docker.com/desktop`
