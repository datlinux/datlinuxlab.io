> [🛒️ Get this book as a PDF or EPUB e-book from Leanpub](https://leanpub.com/numpybyexample).

# Appendix D. Solutions to Exercises

(appendix-d)=

Answers to exercises assume, where required, that numpy has been imported:

```python
import numpy as np
```

*Please note that some of the output and results have been formatted for better display.*

## D.1  Chapter 3

### Exercise 3-1

*Create a new 64 byte `dtype` of float type, using a sized alias, and assign it to the variable `t`. Print out the variable.* 

```python
t = np.dtype('f8')
print(t)
```

```
float64
```

### Exercise 3-2

*Repeat the above `dtype` creation, but instead using an equivalent native Python type.*

```python
t = np.dtype(float)
print(t)
```


```
float64
```

### Exercise 3-3

*Write a Python expression that calculates the area of a circle with radius of 30mm.*

```python
# mm units
radius = 30
# Formula: a = pi * r * r
area_mm_sq = np.pi * (radius**2)
# In units of mm-squared
print(area_mm_sq)
```

```
2827.4333882308138
```

### Exercise 3-4

*Convert the result of the area of the circle to units of cm{sup}`2`. Print the result.*

```python
area_cm_sq = area_mm_sq / (10**2)
print(area_cm)
```

```
28.274333882308138
```

## D.2  Chapter 4

### Exercise 4-1

*Create a 2-D Python list of the test scores, with the student scores as "rows" in test order. Assign this list to the variable `student_scores_list`. Print out this list.* 

```python
student_scores_list = [
 [1, 63.5, 56.0, 68.0, 73.5],
 [2, 53.0, 77.5, 61.0, 83.0],
 [3, 59.0, 79.0, 67.5, 70.0]
]
print(student_scores_list)
```

```
[[1, 63.5, 56.0, 68.0, 73.5], 
 [2, 53.0, 77.5, 61.0, 83.0], 
 [3, 59.0, 79.0, 67.5, 70.0]]
```

### Exercise 4-2

*Using the list from Exercise 4-1, create a NumPy array assigned to `student_scores_arr`, and explicitly assign it an appropriate floating point `dtype`. Print out the array.*

```python
student_scores_arr = np.array(student_scores_list, 
                           dtype=float)
print(student_scores_arr)
```

```
[[ 1.  63.5 56.  68.  73.5],
 [ 2.  53.  77.5 61.  83. ],
 [ 3.  59.  79.  67.5 70. ]]
```

NumPy arrays must be of the same type, in this case whole numbers (integers) were promoted to floating point values.

### Exercise 4-3

*Create a copy of `student_scores_arr` whilst assigning it to a new variable. Change the type of the copied array to a suitable integer. What do the scores look like now? What effect did the conversion have on the values?*

```python
scores_arr_cp = scores_arr.astype(np.int_)
print(scores_arr_cp)
```

```
[[ 1 63 56 68 73]
 [ 2 53 77 61 83]
 [ 3 59 79 67 70]]
```

The effect of the conversion was to round down (take the floor of) the values.

### Exercise 4-4

*Create a new array filled with ones, of the same dimensions as `student_scores_arr`. Print the array. What is the `dtype` of this array?*

```python
arr_ones = np.ones_like(student_scores_arr)
print(arr_ones)
```

```
[[1. 1. 1. 1. 1.]
 [1. 1. 1. 1. 1.]
 [1. 1. 1. 1. 1.]]
```

### Exercise 4-5

*Create an identity matrix of 4x4 size, and print the result.*

```python
arr_ident = np.identity(n=4, dtype=int)
print(arr_ident)
```

```
[[1 0 0 0]
 [0 1 0 0]
 [0 0 1 0]
 [0 0 0 1]]
```

### Exercise 4-6

*Design a suitable named compound type for `student_scores_arr` (using sensible names without spaces), where the student id is an integer, and the scores are a floating point. Recreate the scores array to use this compound type. Print out the `dtype` for the array. (Hint: use tuples as rows.)*

```python
t = np.dtype([
 ('student', 'int'), 
 ('test1', float), 
 ('test2', float), 
 ('test3', float)]) 
# Note the use of tuples as rows..
student_scores_arr = np.array([
    (1, 85.5, 90.0, 78.5, 92.0),  
    (2, 79.0, 88.5, 95.5, 87.0),
    (3, 92.5, 87.0, 89.5, 91.0)], 
    dtype=t)
print(student_scores_arr.dtype)
```

```
dtype([('student', '<i8'), 
 ('test1', '<f8'), 
 ('test2', '<f8'), 
 ('test3', '<f8')])
```

### Exercise 4-7

*Convert the structured array created in Exercise 4-6 to a `recarray` array. Print out the 2{sup}`nd` column of the record array by name.*

```python
scores_rec = student_scores_arr.view(np.recarray)
scores_rec.test1
```

```
array([85.5, 79. , 92.5])
```

## D.3  Chapter 5

### Exercise 5-1

*What is the shape of the NumPy array? Use the array's `shape` property to confirm your conclusion.* 

This is a 2x3 array. This can be confirmed using:

```python
a.shape
```

```
(2, 3)
```

### Exercise 5-2

*What is the size of each element in this array, in bytes?*

```python
i = a.itemsize
print(i)
```

```
8
```

### Exercise 5-3

*Use the appropriate inspection property to find the total bytes consumed by the array. How does this compare to the multiple of the previous exercise's result by the total count of elements?*

```python
b = a.nbytes 
print(b)
```

```
48
```

`size` gives you the number of elements, therefore:

```python
b = i * a.size 
print(b)
```

```
48
```

### Exercise 5-4

*What is the string name of the data type of this array?*

```python
a.dtype.name
```

```
'int64'
```

### Exercise 5-5

*If a third row containing the elements `[7 8 9]` was added to the array, what would be the number of dimensions?*

Adding a new row simply increases the size of the 0{sup}`th` dimension, so the number of dimensions remains unchanged at `2`. The shape, however, becomes `(3,3)`.

```python
a = np.array([(1, 2, 3), (4, 5, 6), (7, 8, 9)]) 
a.ndim
```

```
2
```

```python
a.shape
```

```
(3, 3)
```

## D.4  Chapter 6

### Exercise 6-1

*Load the file into a NumPy array, excluding the header, and print the array.* 

```python
# Set f to the file's path on your computer:
f = '/home/student_scores.csv'
a = np.loadtxt(f, delimiter=',', skiprows=1)
print(a)
```

```
array([[ 1. , 63.5, 56. , 68. , 73.5],
 [ 2. , 53. , 77.5, 61. , 83. ],
 [ 3. , 59. , 79. , 67.5, 70. ]])
```

### Exercise 6-2

*Load the array instead using the compound type. Print the new array.*

```python
from numpy import genfromtxt
r = genfromtxt(f, delimiter=',', dtype=t, names=True)
print(r)
```

```
array([(1, 63.5, 56. , 68. ), 
(2, 53. , 77.5, 61. ),
(3, 59. , 79. , 67.5)],
dtype=[('student_no', '<i8'), 
       ('test_1', '<f8'), 
       ('test_2', '<f8'), 
       ('test_3', '<f8')])
```

### Exercise 6-3

*Convert the array you created in Exercise 6-2 to a `recarray`, and print out a list of the student numbers using the 'array.field' notation.*

```python
r = r.view(np.recarray)
r.student_no
```

```
array([1, 2, 3])
```

## D.5  Chapter 7

### Exercise 7-1

*Get a list of (only) the scores of the 1{sup}`st` student.* 

```python
scores_array[0, 1:]
```

```
array([63.5, 56. , 68. , 73.5])
```

### Exercise 7-2

*Print out student IDs of the 1{sup}`st` two students.*

```python
scores_array[:2, 0]
```

```
array([1., 2.])
```

### Exercise 7-3

*Modify the score of the 2{sup}`nd` student in the second test to 87.*

```python
scores_array[1, 2] = 87
print(scores_array[1,:])
```

```
 [ 2.  53.  87.  61.  83. ]
```

### Exercise 7-4

*Print the scores of all students in the 3{sup}`rd` test.*

```python
scores_array[:, 3]
```

```
array([68. , 61. , 67.5])
```

### Exercise 7-5

*Modify the scores of all students in the 4{sup}`th` test to 75.*

```python
scores_array[:, 4] = 75
print(scores_array)
```

```
[[ 1.  63.5 56.  68.  75. ]
 [ 2.  53.  87.  61.  75. ]
 [ 3.  59.  79.  67.5 75. ]]
```

### Exercise 7-6

*Print the scores of the 2{sup}`nd` student in the last two tests.*

```python
scores_array[1, -2:]
```

```
array([61., 75.])
```

### Exercise 7-7

*Modify the scores of all students in the 1{sup}`st` test to 70.*

```python
scores_array[:, 1] = 70
print(scores_array)
```

```
[[ 1.  70.  56.  68.  75. ]
 [ 2.  70.  87.  61.  75. ]
 [ 3.  70.  79.  67.5 75. ]]
```

### Exercise 7-8

*Print the ID and scores of the last student.*

```python
scores_array[-1,0]
scores_array[-1, 1:]
```

```
3.0
array([70. , 79. , 67.5, 75. ])
```

### Exercise 7-9

*Repeat the selection from Example 7-8 but assign it to the variable `sub_arr`. Change the first score of `sub_arr` to 77. Is this sub-selection a view? Confirm by printing both `sub_arr` and `scores_array` to see if the original array has also been modified.*

```python
sub_arr = scores_array[-1, 1:]
sub_arr[0] = 77
print(sub_arr)
print(scores_array)
```

`sub_arr` is a view, you can see that both arrays have been modified:

```
array([77. , 79. , 67.5, 75. ])

array([[ 1. , 70. , 56. , 68. , 75. ],
 [ 2. , 70. , 87. , 61. , 75. ],
 [ 3. , 77. , 79. , 67.5, 75. ]])
```

### Exercise 7-10

*Retrieve an array of the scores only (no student id) and apply a conditional expression to return True|False for any scores over 80.*

```python
scores_only = scores_array[:, 1:]
scores_over_80 = scores_only > 80
print(scores_over_80)
```

```
[[False False False False]
 [False False False  True]
 [False False False False]]
 ```

## D.6  Chapter 8

### Exercise 8-1

*Perform element-wise addition (+) and multiplication (\*) of the two arrays and print the results.* 

```python
# Element-wise addition using ufunc
addition_result = np.add(a, b)
print("Addition Result:", addition_result)
# Element-wise multiplication using ufunc
multiplication_result = np.multiply(a, b)
print("Multiplication Result:", multiplication_result)
```

```
Addition Result: [5 7 9]

Multiplication Result: [ 4 10 18]
```

### Exercise 8-2

*Does the matrix `dot` operation between the arrays result in a valid array? Print the result. Can you explain how the result was generated?*

```python
result = a.dot(b)
```

```
32
```

No. A `dot` operation on a 1x3 matrix with another 1x3 matrix results in a scalar value. This can be depicted as follows:

\begin{align}
\begin{pmatrix}
1 \\
2 \\
3
\end{pmatrix}
\ldotp
\begin{pmatrix}
4 \\
5 \\
6
\end{pmatrix}
= 
\begin{pmatrix}
32
\end{pmatrix}
\end{align}

Using `dot` on two equal size 1-D arrays (n-vectors) performs a matrix dot-product (or scalar-product) operation. (This should not be confused with "scalar * array" or "array * array" arithmetic multiplication.) However, if one side of the `dot` operation in NumPy *is* a scalar value, then it will revert to "scalar * array", or "array * array" multiplication if the vectors are of different size.

### Exercise 8-3

*Perform element-wise comparison (greater than, less than, equal to) between the elements of these arrays. Print the result of each comparison.*

```python
greater_than_result = np.greater(a, b)
less_than_result = np.less(a, b)
equal_to_result = np.equal(a, b)
print("Greater Than Result:", greater_than_result)
print("Less Than Result:", less_than_result)
print("Equal To Result:", equal_to_result)
```

```
Greater Than Result: [False False False]

Less Than Result: [ True  True  True]

Equal To Result: [False False False]
```

### Exercise 8-4

*Perform scalar division on the array `a` with the value 5. Print the resulting array. What is the `dtype` of the result?*

```python
result = a / 5
print(result)
print(result.dtype)
```

```
array([0.2, 0.4, 0.6])

dtype('float64')
```

### Exercise 8-5

*Create a larger NumPy array `b` with dimensions (3, 3) containing random values. Then, add array `a` to  `b`. Print the resulting array.*

```python
b = np.random.randint(0, 10, size=(3, 3))
result = a + b
print(b)
print(result)
```

```
[[8 7 6]
 [0 8 8]
 [1 0 0]]

[[ 9  9  9]
 [ 1 10 11]
 [ 2  2  3]]
```

Note, your final result will vary depending on the random values generated in `b`. The result is determined by adding `a` (`[1, 2, 3]`) to each row of `b`.

### Exercise 8-6

*What percentage of student scores are over 80?*

```python
scores_only = scores_array[:, 1:]
scores_over_80 = scores_only > 80
num_scores_over_80 = np.sum(scores_over_80)
perc_over_80 = (num_scores_over_80 / scores_only.size) 
            * 100
print(scores_over_80)
print("Percentage of student scores over 80:", 
   perc_over_80, "%")
```

```
[[False False False False]
 [False False False  True]
 [False False False False]]

Percentage of student scores over 80: 8.333333333333332 %
```

### Exercise 8-7

*Compute the mean and standard deviation of the NumPy array `arr = np.array([1, 2, 3, 4, 5])`.*

```python
mean_value = np.mean(arr)
std_deviation = np.std(arr)
print("Mean:", mean_value)
print("Standard Deviation:", std_deviation)
```

```
Mean: 3.0

Standard Deviation: 1.4142135623730951
```

### Exercise 8-8

*Compute and print out the median and quartiles (25th and 75th percentiles) of the following NumPy array `arr = np.array([10, 20, 30, 40, 50])`.*

```python
median_value = np.median(data)
quartiles = np.percentile(data, [25, 75])
print("Median:", median_value)
print("25th Percentile (Q1):", quartiles[0])
print("75th Percentile (Q3):", quartiles[1])
```

```
Median: 30.0

25th Percentile (Q1): 20.0

75th Percentile (Q3): 40.0
```

### Exercise 8-9

*Compute and print out the correlation coefficient between the two NumPy arrays `x` and `y`.*

```python
corr_coefficient = np.corrcoef(x, y)[0, 1]
print("Correlation coefficient:", corr_coefficient)
```

```
Correlation coefficient: -0.9881395367446534
```

The correlation coefficient is very near to -1, therefore there is a strong negative correlation; as `x` increases, `y` decreases linearly.

## D.7  Chapter 9

### Exercise 9-1

*Convert the array `a = np.array([[1, 2], [3, 4]])`* 

```python
a = np.array([[1, 2], [3, 4]])
a.transpose()
```

```
array([[1, 3],
 [2, 4]])
```

### Exercise 9-2

*Is it possible to reshape the 2x2 array `a = np.array([[1, 2], [3, 4]])` into a 4x1 array? If so what is the procedure? Assign this new array to variable `b`.*

```python
a = np.array([[1, 2], [3, 4]])
b = np.reshape(a, (4,1))
print(b)
```

```
array([[1],
 [2],
 [3],
 [4]])
```

### Exercise 9-3

*Flatten the arrays `a` and `b` from exercise 2, and combine them into a single 2x4 array assigned to `c`.*

```python
a = np.ravel(a)
b = np.ravel(b)
c = np.vstack((a, b))
```

```
array([[1, 2, 3, 4],
 [1, 2, 3, 4]])
```

### Exercise 9-4

*Use a rotate operation upon `c` to convert it into a 2x4 array.*

```python
# Using `c` from previous exercise..
np.rot90(c, 1)
```

```
array([[4, 4],
 [3, 3],
 [2, 2],
 [1, 1]])
```

### Exercise 9-5

*Convert the 1-D array `a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])` into a 3x3 array sorted in reverse order.*

The key is to reverse the array first:

```python
a = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
a = np.flip(a)
np.split(a, 3)
```

```
array([[9, 8, 7],
 [6, 5, 4],
 [3, 2, 1]])
```
