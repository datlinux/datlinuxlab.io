> [🛒️ Get this book as a PDF or EPUB e-book from Leanpub](https://leanpub.com/numpybyexample).

# Appendix B. Python Lists & `array` Vs NumPy Arrays

(appendix-b)=

Python lists are a native language feature that let you easily create array-like data sequences, using bracket (`[]`) indexing to access the data. Lists are also expandable — you can add and remove items at any time. Whilst flexible, they have limitations, and the `array` object is available to use if lists don't meet your engineering goals. For advanced data processing, however, the NumPy array (`ndarray`) may be the superior choice. 

When you think of arrays, you typically expect them to have the following characteristics: 

1. Be of a fixed size

2. Be of the same type

3. Have efficient indexing

4. Ability to efficiently operate on them

5. Multidimensional structure.

We'll discuss lists, the `array` module, and NumPy arrays in context of these features, and highlight how they differ and when you might prefer one over the others.

## B.1  Python Lists

Python lists are simple to create, for example:  

```python
items = [1, 2.5, 'N/A', [555, 'FILK']]
items
```

```
[1, 2.5, 'N/A', [555, 'FILK']]
```

This is a perfectly valid Python list, but you can immediately see that a list can be made up of any type. Lists, therefore, put the responsibility on the engineer to enforce the rules of typing. And whilst it's perfectly acceptable to stick to a convention, the lack of language-level enforcement can lead to problems simply because nothing prevents the list from being created with the wrong types in the first place.  

Lists are also expandable, and therefore don't have a fixed size: 

```python
items.pop(3)
items
```

```
[1, 2.5, 'N/A']
```

So far we've broken the first two "rules" of the array test. But what about indexing? Lists are easily indexed, which meets goal 3. But without enforcement, you may not know what type you're getting for certain. On top of that, in order to operate on a list in an element-wise fashion, you need to iterate over the entire list, for example:  

```python
for i in items:
    print(i)
```

```
1
2.5
N/A
```

You can be a little more expressive with list comprehensions:  

```python
[i for i in items]
```

```
[1, 2.5, 'N/A']
```

But at the end of the day, it's just a more concise way of looping over a list. 

Furthermore, if you want to operate on a list you need to be sure to handle potential problems with types, as they may not be compatible with the operation, for example:  

```python
[i * 2 for i in items]
```

```
[2, 5.0, 'N/AN/A']
```

This example ostensibly performs a multiplication operation on the data, and this time gets a result without failing; but the resulting value may vary unexpectedly depending on an element's type (`*` is an overloaded operator and works with strings differently to numbers) — or it may fail entirely.

Given that lists can be made up of other lists, you *can* design them to have a multidimensional structure:  

```python
multidim = [[1,2,3], [4,5,6]]
```

And this looks a lot like a NumPy array — in fact you could use this array-like object to create a NumPy array, as we've seen before in this book. But relying on lists for n-D arrays adds to the complexity of enforcing types and managing dimension sizes. They soon become inefficient (multiple loops and complex logic) and unreliable.  

## B.2  The `array` Array

The `array` module ships with Python as a core library feature, so you don't have to install it using `pip`, but you do need to import it:  

```python
import array
```

To create an `array` you are required to specify the type, and there are thirteen available type codes defined. Here are a few of them:

- `'b'` — a `char`
- `'i'` — a signed `int` of 2 bytes
- `'l'` — a signed `long` of 4 bytes
- `'f'` — a `float` of 4 bytes
- `'d'` — a `double` of 8 bytes

You can also run `array.typecodes` to get a quick print out of all codes:  

```python
array.typecodes
```

```
'bBuhHiIlLqQfd'
```

```{note}
Use the handy `help()` feature to get useful information on any function. `help(array)` will output information about `array` usage, along with a full listing of type codes with more detail.  

`help(array)`
```

```
NAME
array

DESCRIPTION
This module defines an object type which can 
efficiently represent an array of basic values: 
characters, integers, floating point numbers. 
Arrays are sequence types and behave very much
like lists, except that the type of objects 
stored in them is constrained.

CLASSES
builtins.object
    array

ArrayType = class array(builtins.object)
| array(typecode [, initializer]) -> array
|  
| Return a new array whose items are restricted by 
| typecode, and initialized from the optional 
| initializer value, which must be a list, string or 
| iterable over elements of the appropriate type.
|  
| Arrays represent basic values and behave very much 
| like lists, except the type of objects stored in 
| them is constrained. The type is specified at object
| creation time by using a type code, which is a 
| single character.
|
| The following type codes are defined:
|  
|      Type code   C Type             Min size (bytes)
|      'b'         signed integer     1
|      'B'         unsigned integer   1
|      'u'         Unicode character  2 (see note)
|      'h'         signed integer     2
|      'H'         unsigned integer   2
|      'i'         signed integer     2
|      'I'         unsigned integer   2
|      'l'         signed integer     4
|      'L'         unsigned integer   4
|      'q'         signed integer     8 (see note)
|      'Q'         unsigned integer   8 (see note)
|      'f'         floating point     4
|      'd'         floating point     8

*(Output truncated)*
```

Creating an array is done as follows (first argument is the type code):  

```python
a = array('i', [1, 2, 3, 4, 5])
a
```

```
array('i', [1, 2, 3, 4, 5])
```

You have overcome the problem of loose typing with `array` arrays, and have at least created a structure of a certain size up front. But the `array` is not designed to be multidimensional — unless you create a list of `array` arrays:  

```python
b = array('i', [1, 2, 3])
c = array('f', [2., 3., 4.])
a = [b, c];
a
```

```
[array('i', [1, 2, 3]), 
 array('f', [2.0, 3.0, 4.0])]
```

Overcoming one problem, however, just re-introduces the problem that you had to begin with — lists. And you now have two different structures to deal with when processing.

```{note}
In terms of resource efficiency, an `array` is better at memory allocation, because it's given the type (and therefore the size of memory to allocate to an element), and the size of the array — so it knows the amount of memory to assign to the entire array object. Where a list may perform better than an `array` array is with adding and removing elements, simply because a list will over-allocate memory and this may provide a processing efficiency benefit on average. 

However, if you expect to be doing a lot of insert, append, or remove operations, then neither lists nor arrays may be the best option. In that case you should look at more advanced structures such as hash tables, or use an appropriate database. A library like Pandas is also an excellent choice if you need to manipulate table-like DataFrames. 
```

```{note}
It's not entirely true that `array` is of a fixed size, because it does have `append` and `pop` functions that will resize it. In this sense, it's more like a Java `ArrayList`, for those who are familiar with Java programming.
```

## B.3  The Case for (or Against) NumPy Arrays

The entire book has been dedicated to explaining the importance and use of `ndarray` arrays in NumPy, so there's no need to re-state the case at any length. And given the limitations of lists or `array` arrays, it should now be clearer if and when you might decide to upgrade from one approach to the next.  

If you need a simple data structure that's easy to create, is mutable, that you can take responsibility over typing, and you won't be performing very complex operations on, then stick with lists. Lists are highly inter-operable, and are easy to work with especially along with list comprehensions or generators.  

An `array` is something of an improvement on lists — you could call them a wrapper of simple lists that enforces type checking. Another reason to prefer an `array` over a list is when you might need to persist simple data structures to file storage. It would be prudent to have type safety in this case, especially if the stored data will be shared among software components.   

For everything else, use NumPy. Or, find a library such as SciPy or Pandas that builds on NumPy to provide the more specialised capabilities you're after. 
