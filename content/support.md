---
title: Help
subtitle: 💬️ Help, announcements, feedback, and contact
comments: false
---

___

### User support
### 

#### Community

Visit the DAT Linux **Github Community** channel and post any feedback, issues,
or general questions related to DAT Linux: [https://github.com/dat-linux/community/discussions](https://github.com/dat-linux/community/discussions/).


#### Email

`info ꩜ datlinux • com`

#### Mastodon

[https://fosstodon.org/deck/@lsm](https://fosstodon.org/deck/@lsm)

___ 

### Other ways to show your support:

{{< rawhtml >}}
<table>
<tr style="background-color:#FFCCCC">
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license. <a href="/pro">Get PRO now!</a>
</td>
</tr>
</table>    
<br/>
{{< /rawhtml >}}

{{< rawhtml >}}
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:90px;padding:5px;background-color:#e4e4e4;">
    <img style="width:90px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:5px;color:#333333;">
    <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team. <a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/"> 
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
