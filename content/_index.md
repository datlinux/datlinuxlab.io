#####

{{< rawhtml >}}
<table style=margin-top:20px;margin-bottom:20px;>
<tr style="background-color:#FFCCCC">
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license.<br/><a href="/pro">Get PRO now! (🎓 Student & 🏫 Educator discounts!!)</a>
    <br/>
</td>
</tr>
</table>
{{< /rawhtml >}}

___ 

## Introduction

DAT Linux is a **Linux distribution for data science**. It brings together all
your favourite *open-source* data science tools and apps into a ready-to-run
desktop environment. It's based on Ubuntu 24.04 LTS, so it's easy to install and use. The custom 
**DAT Linux Control Panel** provides a centralised *one-stop-shop* for running 
and managing dozens of data science programs. [Read the FAQ](/faq).

DAT Linux is perfect for students, professionals, academics, or anyone
interested in data science who doesn't want to spend endless hours
downloading, installing, configuring, and maintaining applications from a range
of sources, each with different technical requirements and set-up challenges.

{{< rawhtml >}}
<br/>
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:80px;padding:0px;background-color:#e4e4e4;">
    <img style="width:80px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:7px;color:#333333;">
    Featured <a href="/ebooks">e-book</a>: <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team.
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

___ 

## Screenshots

{{< gallery dir="/album/" caption-effect="fade" />}}

___ 

## Get started

- [⬇️ Download](/download) DAT Linux, and get on with doing data science
without the headaches.

- [ℹ️ FAQ](/faq) for more answers to some questions you may have.

- [💬️ Github Community](https://github.com/dat-linux/community/discussions)
channel for annoucements, or to post any feedback, issues or general queries.

___ 

## List of supported data science apps

- [Suggest an app](https://github.com/dat-linux/community/discussions/categories/app-extra-book-suggestions).

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)

|     | App |  Description  |
| --- | --- |  -----------  |
|{{< figure src="/img/alphaplot.png" >}}            | AlphaPlot         | AlphaPlot is an open-source computer program for interactive scientific graphing and data analysis. |
|{{< figure src="/img/birt.png" >}}                 | BiRT              | Eclipse BIRT™ is an open source reporting system for producing compelling BI reports |
|{{< figure src="/img/clickhouse.png" >}}           | ClickHouse        | ClickHouse is an open-source column-oriented DBMS for online analytical processing |
|{{< figure src="/img/datacleaner.png" >}}          | Data Cleaner      | Data Quality toolkit that allows you to profile, correct, and enrich your data |
|{{< figure src="/img/datasette.png" >}}            | Datasette         | Datasette is a tool for exploring and publishing data visually and with SQL |
|{{< figure src="/img/dbbrowser.png" >}}            | DB Browser        | DB Browser for SQLite is a visual, open source tool to create, design, and edit database files compatible with SQLite |
|{{< figure src="/img/dbeaver.png" >}}              | DBeaver           | Free multi-platform database tool for developers, database administrators, analysts and all people who need to work with databases |
|{{< figure src="/img/druid.png" >}}                | Druid             | Apache Druid is a real-time database to power modern analytics applications  |
|{{< figure src="/img/dsearch.png" >}}              | D-Search          | Convenient interface to the "webtools" R package to search for datasets in --all-- CRAN packages |
|{{< figure src="/img/duckdb.png" >}}               | DuckDB            | DuckDB is an in-process SQL OLAP database management system |
|{{< figure src="/img/gephi.png"  >}}               | Gephi             | Gephi is the leading visualization and exploration software for all kinds of graphs and networks  |
|{{< figure src="/img/glueviz.png"  >}}             | Glue-viz          | Glue is a UI and Python library to explore relationships within and among related datasets  |
|{{< figure src="/img/grafana.png" >}}              | Grafana           | Grafana is a popular open-source platform for data visualization and monitoring |
|{{< figure src="/img/gvim.png" >}}                 | G-Vim             | A GUI wraper for the Vim screen-based text editor program, with plugins for R installed |
|{{< figure src="/img/ipython.png" >}}              | IPython           | A command shell for interactive computing with a convenient console launcher |
|{{< figure src="/img/julia.png" >}}                | Julia             | Julia is a high-level, high-performance, dynamic programming language |
|{{< figure src="/img/jamovi.png" >}}               | Jamovi            | jamovi is a new “3rd generation” statistical spreadsheet. And a compelling alternative to SPSS and SAS.  |
|{{< figure src="/img/jasp.png" >}}                 | JASP              | JASP is a cross platform statistical software program with a state-of-the-art graphical user interface. |
|{{< figure src="/img/jupyter.png" >}}              | Jupyter Notebook  | The Jupyter Notebook is a web-based interactive, scientific computing platform |
|{{< figure src="/img/jupyter_lab.png" >}}          | Jupyter Lab       | JupyterLab is the latest web-based interactive development environment for notebooks, code, and data |
|{{< figure src="/img/knime.png" >}}                | KNIME             | KNIME Analytics Platform is open source software for data science |
|{{< figure src="/img/kst.png" >}}                  | kst               | Kst is a fast real-time large-dataset viewing and plotting tool available with built-in data analysis. |
|{{< figure src="/img/labplot.png" >}}              | LabPlot           | Free, open source and cross-platform Data Visualization and Analysis software accessible to everyone  |
|{{< figure src="/img/librecalc.png" >}}            | LibreOffice Calc  | LibreOffice Calc is the spreadsheet component of the LibreOffice software package |
|{{< figure src="/img/luigi.png" >}}                | Luigi             | Luigi provides a framework to develop and manage data processing pipelines |
|{{< figure src="/img/meld.png" >}}                 | Meld              | Meld is a visual file diff and merge tool |
|{{< figure src="/img/metabase.png" >}}             | Metabase          | Metabase is an open-source business intelligence tool |
|{{< figure src="/img/moa.png" >}}                  | MOA               | MOA is an open source framework for Big Data stream mining. It includes a collection of machine learning algorithms  |
|{{< figure src="/img/openrefine.png" >}}           | OpenRefine        | OpenRefine is an open-source desktop application for data cleanup and transformation to other formats |
|{{< figure src="/img/orange.png" >}}               | Orange            | Orange is a powerful platform to perform data analysis and visualization |
|{{< figure src="/img/paraview.png" >}}             | Paraview          | ParaView is an open-source, multi-platform data analysis and visualization application |
|{{< figure src="/img/pluto.png" >}}                | Pluto             | A Pluto notebook is made up of small blocks of Julia code (cells) and together they form a reactive notebook |
|{{< figure src="/img/pspp.png" >}}                 | PSPP              | GNU PSPP is a program for statistical analysis of sampled data. It is a free as in freedom replacement for the proprietary program SPSS |
|{{< figure src="/img/qgis.png" >}}                 | QGIS              | QGIS is a Free and Open Source Geographic Information System |
|{{< figure src="/img/quarto.png" >}}               | Quarto            | Quarto® is an open-source scientific and technical publishing system built on Pandoc |
|{{< figure src="/img/R.png" >}}                    | R                 | R is a free software environment for statistical computing and graphics |
|{{< figure src="/img/rstudio.png" >}}              | R-Studio          | RStudio is an Integrated Development Environment (IDE) for R |
|{{< figure src="/img/scilab.png" >}}               | Scilab            | Scilab is a free and open-source, cross-platform numerical computational package and a high-level, numerically oriented programming language |
|{{< figure src="/img/spyder.png" >}}               | Spyder            | Spyder is a free and open source scientific environment written in Python, for Python, and designed by and for scientists, engineers and data analysts |
|{{< figure src="/img/superset.png" >}}             | Superset          | Apache Superset is a modern, enterprise-ready business intelligence web application |
|{{< figure src="/img/tabula.png" >}}               | Tabula            | Tabula is a free tool for extracting data from PDF files into CSV and Excel files |
|{{< figure src="/img/veusz.png" >}}                | Veusz             | Veusz is a scientific plotting and graphing program with a graphical user interface, designed to produce publication-ready 2D and 3D plots
|{{< figure src="/img/visidata.png" >}}             | Visidata          | Visidata is an interactive multitool for tabular data. It combines the clarity of a spreadsheet, the efficiency of the terminal, and the power of Python, which can handle millions of rows with ease
|{{< figure src="/img/vscodium.png" >}}             | VSCodium          | VSCodium is a community-driven, freely-licensed binary distribution of Microsoft's editor VS Code (ready with plugins for R/RMarkdown, Python/Jupyter, Julia) |
|{{< figure src="/img/weka.png" >}}                 | Weka              | Weka is a GUI and collection of machine learning algorithms for data mining tasks |
|{{< figure src="/img/wxmaxima.png" >}}             | WxMaxima          | wxMaxima is a document based interface for the computer algebra system Maxima |
|{{< figure src="/img/zeppelin.png" >}}             | Zeppelin          | Web-based notebook that enables data-driven, interactive data analytics and collaborative documents with SQL, Scala, Python, R and more |
