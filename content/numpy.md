---
title: NUMPY BY EXAMPLE
subtitle: A Beginner's Guide to Learning NumPy - by DAT Linux
comments: false
---

___

The book was designed to introduce NumPy features in a structured way, with each chapter building on your knowledge incrementally.

- 📖️ [Read it free online](https://datlinux.com/numpybyexample/_build/html/)
- 🛒 [Buy the PDF or EPUB E-Book](https://leanpub.com/numpybyexample)
- 📎 [Download the companion Jupyter notebooks](/NUMPY-BY-EXAMPLE-CODE.zip)

![](/book.png)

___ 

### Other ways to show your support:

{{< rawhtml >}}
<table>
<tr style="background-color:#FFCCCC">
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license. <a href="/pro">Get PRO now!</a>
</td>
</tr>
</table>
<br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
