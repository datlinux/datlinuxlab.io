---
title: E-Books
subtitle: 📚️ Free Online Data Science E-Books
comments: false
---

{{< rawhtml >}}
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:80px;padding:0px;background-color:#e4e4e4;">
    <img style="width:80px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:7px;color:#333333;">
    Featured book: <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team.
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

___ 

This is the online list of the same curated data science e-books that you'll find in the DAT Linux Control Panel "Extras" tab, as well as enhanced features to filter and favourite e-books with a [PRO](/pro) tools license. To report an error or suggest a book, email `info` AT `datlinux` DOT `com`.

___ 

{{< rawhtml >}}
<div id="booksbtns" "class="btn-group" role="group" aria-label="Basic example">
  <button id="all" "type="button"         class="btn btn-primary">🔃️ RESET (ALL)</button>
  <button id="analytics" type="button"    class="btn btn-secondary">Analytics</button>
  <button id="apps" type="button"         class="btn btn-secondary">Apps</button>
  <button id="bi" type="button"           class="btn btn-secondary">BI</button>
  <button id="biz" type="button"          class="btn btn-secondary">Business</button>
  <button id="bigdata" type="button"      class="btn btn-secondary">Big data</button>
  <button id="bio" type="button"          class="btn btn-secondary">Bio</button>
  <button id="dataanalysis" type="button" class="btn btn-secondary">Data analysis</button>
  <button id="mining" type="button"       class="btn btn-secondary">Data mining</button>
  <button id="datsci" type="button"       class="btn btn-secondary">Data science</button>
  <button id="vis" type="button"          class="btn btn-secondary">Data vis</button>
  <button id="db" type="button"           class="btn btn-secondary">DBs</button>
  <button id="deep" type="button"         class="btn btn-secondary">Deep learning</button>
  <button id="devops" type="button"       class="btn btn-secondary">Dev-ops</button>
  <button id="fin" type="button"          class="btn btn-secondary">Finance</button>
  <button id="gis" type="button"          class="btn btn-secondary">GIS</button>
  <button id="hum" type="button"          class="btn btn-secondary">Humanities</button>
  <button id="inference" type="button"    class="btn btn-secondary">Inference</button>
  <button id="julia" type="button"        class="btn btn-secondary">Julia</button>
  <button id="jupyter" type="button"      class="btn btn-secondary">Jupyter</button>
  <button id="math" type="button"         class="btn btn-secondary">Math</button>
  <button id="ml" type="button"           class="btn btn-secondary">ML</button>
  <button id="nlp" type="button"          class="btn btn-secondary">NLP</button>
  <button id="practice" type="button"     class="btn btn-secondary">Practice</button>
  <button id="prob" type="button"         class="btn btn-secondary">Probability</button>
  <button id="python" type="button"       class="btn btn-secondary">Python</button>
  <button id="rlang" type="button"        class="btn btn-secondary">R</button>
  <button id="regression" type="button"   class="btn btn-secondary">Regression</button>
  <button id="stats" type="button"        class="btn btn-secondary">Statistics</button>
  <span class="label label-default">Books found: <span class="badge" id="bookmatch">0</span></span>
</div>

<hr/>
<style>
 a.buy { color:black; font-weight:bold; background-color:yellow; }
 button.btn { margin-top: 5px; }
</style>
<div class="abook dataanalysis analytics biz"><span style="font-size:2em"> 📔️ </span><b>A Business Analyst’s Introduction to Business Analytics</b> - Fleischhacker [<a target="_blank" href="https://www.causact.com/#welcome">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>A Crash Course in Geographic Information Systems (GIS) using R</b> - Branion-Calles [<a target="_blank" href="https://bookdown.org/michael_bcalles/gis-crash-course-in-r/">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>Advanced R</b> - Wickham [<a target="_blank" href="https://adv-r.hadley.nz/">📖️ Read</a>]<hr/></div>
<div class="abook math"><span style="font-size:2em"> 📔️ </span><b>A First Course in Linear Algebra</b> - Beezer [<a target="_blank" href="http://linear.ups.edu/html/fcla.html">📖️ Read</a>]<hr/></div>
<div class="abook stats inference"><span style="font-size:2em"> 📔️ </span><b>A First Course on Statistical Inference</b> - Peralta, García-Portugués [<a target="_blank" href="https://bookdown.org/egarpor/inference/">📖️ Read</a>]<hr/></div>
<div class="abook devops datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Agile Data Science with R</b> - Edwin Thoen [<a target="_blank" href="https://edwinth.github.io/ADSwR/">📖️ Read</a>]<hr/></div>
<div class="abook rlang bio"><span style="font-size:2em"> 📔️ </span><b>A Little Book of R for Bioinformatics 2.0</b> - Avril Coghlan [<a target="_blank" href="https://brouwern.github.io/lbrb/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis regression rlang"><span style="font-size:2em"> 📔️ </span><b>Analysing Data using Linear Models: With applications in R</b> - van den Berg [<a target="_blank" href="https://bookdown.org/pingapang9/linear_models_bookdown/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis rlang"><span style="font-size:2em"> 📔️ </span><b>Analysing Quantitative Data with R</b> - University of Warwick [<a target="_blank" href="https://drfloreiche.github.io/index.html">📖️ Read</a>]<hr/></div>
<div class="abook stats rlang dataanalysis vis"><span style="font-size:2em"> 📔️ </span><b>Analyzing US Census Data: Methods, Maps, and Models in R</b> - K. Walker [<a target="_blank" href="https://walker-data.com/census-r/">📖️ Read</a>]<hr/></div>
<div class="abook analytics bi rlang vis"><span style="font-size:2em"> 📔️ </span><b>Analytics with Power BI and R</b> - Etaati [<a target="_blank" href="https://radacad.com/online-book-analytics-with-power-bi-and-r">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>An Introduction to Bayesian Thinking</b> - Clyde, Çetinkaya-Rundel, et al [<a target="_blank" href="https://statswithr.github.io/book/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis rlang"><span style="font-size:2em"> 📔️ </span><b>An Introduction to Data Analysis</b> - Michael Franke [<a target="_blank" href="https://michael-franke.github.io/intro-data-analysis/index.html">📖️ Read</a>]<hr/></div>
<div class="abook gis"><span style="font-size:2em"> 📔️ </span><b>An Introduction to Earth and Environmental Data Science</b> - Ryan Abernathey [<a target="_blank" href="https://earth-env-data-science.github.io/intro.html">📖️ Read</a>]<hr/></div>
<div class="abook gis dataanalysis"><span style="font-size:2em"> 📔️ </span><b>An Introduction to Spatial Data Analysis and Statistics: A Course in R</b> - Paez [<a target="_blank" href="https://paezha.github.io/spatial-analysis-r/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml"><span style="font-size:2em"> 📔️ </span><b>An Introduction to Statistical Learning 2ed</b> - James, Witten, et al [<a target="_blank" href="https://www.statlearning.com/">📖️ Read</a>]<hr/></div>
<div class="abook stats hum"><span style="font-size:2em"> 📔️ </span><b>Answering questions with data: Introductory Statistics for Psychology Students</b> - Navarro, Suzuki [<a target="_blank" href="https://www.crumplab.com/statistics/">📖️ Read</a>]<hr/></div>
<div class="abook stats rlang"><span style="font-size:2em"> 📔️ </span><b>Applied Statistics with R</b> - David Dalpiaz [<a target="_blank" href="https://book.stat420.org/">📖️ Read</a>]<hr/></div>
<div class="abook python"><span style="font-size:2em"> 📔️ </span><b>A Whirlwind Tour of Python</b> - Jake VanderPlas [<a target="_blank" href="https://jakevdp.github.io/WhirlwindTourOfPython/">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>Bayes Rules! An Introduction to Applied Bayesian Modeling</b> - Johnson, Ott, Dogucu [<a target="_blank" href="https://www.bayesrulesbook.com/">📖️ Read</a>]<hr/></div>
<div class="abook stats regression"><span style="font-size:2em"> 📔️ </span><b>Beyond Multiple Linear Regression</b> - Roback & Legler [<a target="_blank" href="https://bookdown.org/roback/bookdown-BeyondMLR/">📖️ Read</a>]<hr/></div>
<div class="abook bigdata analytics"><span style="font-size:2em"> 📔️ </span><b>Big Data Analytics</b> - Ulrich Matter [<a target="_blank" href="https://umatter.github.io/BigData/">📖️ Read</a>]<hr/></div>
<div class="abook bigdata hum"><span style="font-size:2em"> 📔️ </span><b>Big Data and Social Science</b> - Foster, Ghani, et al [<a target="_blank" href="https://textbook.coleridgeinitiative.org/">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>Authoring Books and Technical Documents with R Markdown</b> - Yihui Xie [<a target="_blank" href="https://bookdown.org/yihui/bookdown/">📖️ Read</a>]<hr/></div>
<div class="abook datsci inference"><span style="font-size:2em"> 📔️ </span><b>Computational and Inferential Thinking: The Foundations of Data Science</b> - 2ed. Adhikari, DeNero, & Wagner [<a target="_blank" href="https://inferentialthinking.com/chapters/intro.html">📖️ Read</a>]<hr/></div>
<div class="abook bio rlang"><span style="font-size:2em"> 📔️ </span><b>Computational Genomics with R</b> - Altuna Akalin [<a target="_blank" href="https://compgenomr.github.io/book/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis python"><span style="font-size:2em"> 📔️ </span><b>Data analysis with Python (Summer 2021)</b> - University of Helsinki [<a target="_blank" href="https://csmastersuh.github.io/data_analysis_with_python_summer_2021/">📖️ Read</a>]<hr/></div>
<div class="abook analytics"><span style="font-size:2em"> 📔️ </span><b>Data Analytics: A Small Approach</b> - Huang, Deng [<a target="_blank" href="https://dataanalyticsbook.info/">📖️ Read</a>]<hr/></div>
<div class="abook mining ml"><span style="font-size:2em"> 📔️ </span><b>Data Mining and Machine Learning</b> - Zaki, Meira, et al [<a target="_blank" href="https://dataminingbook.info/book_html/">📖️ Read</a>]<hr/></div>
<div class="abook datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Data Science: A First Introduction</b> - Timbers, Campbell, & Lee [<a target="_blank" href="https://datasciencebook.ca/">📖️ Read</a>]<hr/></div>
<div class="abook datsci hum rlang"><span style="font-size:2em"> 📔️ </span><b>Data Science for Psychologists</b> - Hansjörg Neth [<a target="_blank" href="https://bookdown.org/hneth/ds4psy/">📖️ Read</a>]<hr/></div>
<div class="abook datsci biz"><span style="font-size:2em"> 📔️ </span><b>Data Science for Startups</b> - Ben G Weber [<a target="_blank" href="https://bgweber.github.io/">📖️ Read</a>]<hr/></div>
<div class="abook datsci hum rlang"><span style="font-size:2em"> 📔️ </span><b>Data Science in Education Using R</b> - Estrellado, Bovee, et al [<a target="_blank" href="https://datascienceineducation.com/">📖️ Read</a>]<hr/></div>
<div class="abook datsci julia"><span style="font-size:2em"> 📔️ </span><b>Data Science in Julia for Hackers</b> - Carrone, Nicolini, & Obst Demaestri [<a target="_blank" href="https://datasciencejuliahackers.com/">📖️ Read</a>]<hr/></div>
<div class="abook datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Data Science Live Book</b> - Pablo Casas [<a target="_blank" href="https://livebook.datascienceheroes.com/">📖️ Read</a>]<hr/></div>
<div class="abook datsci practice"><span style="font-size:2em"> 📔️ </span><b>Data Science Practice</b> - Stephenson [<a target="_blank" href="https://datasciencepractice.study/">📖️ Read</a>]<hr/></div>
<div class="abook vis"><span style="font-size:2em"> 📔️ </span><b>Data Visualization: A Practical Introduction</b> - Kieran Healy [<a target="_blank" href="https://socviz.co/">📖️ Read</a>]<hr/></div>
<div class="abook vis rlang"><span style="font-size:2em"> 📔️ </span><b>Data Visualization with R</b> - Rob Kabacoff [<a target="_blank" href="https://rkabacoff.github.io/datavis/">📖️ Read</a>]<hr/></div>
<div class="abook deep ml"><span style="font-size:2em"> 📔️ </span><b>Deep Learning</b> - Goodfellow, Bengio, & Courville [<a target="_blank" href="https://www.deeplearningbook.org/">📖️ Read</a>]<hr/></div>
<div class="abook datsci practice devops apps"><span style="font-size:2em"> 📔️ </span><b>Designing and Building Data Science Solutions</b> - Van Otten [<a target="_blank" href="https://datasciencedesign.com/">📖️ Read</a>]<hr/></div>
<div class="abook deep"><span style="font-size:2em"> 📔️ </span><b>Dive into Deep Learning</b> - Zhang, Aston, et al [<a target="_blank" href="https://d2l.ai/">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>Efficient R Programming</b> - Gillespie & Lovelace [<a target="_blank" href="https://csgillespie.github.io/efficientR/">📖️ Read</a>]<hr/></div>
<div class="abook rlang apps"><span style="font-size:2em"> 📔️ </span><b>Engineering Production-Grade Shiny Apps</b> - Fay, Rochette, et al [<a target="_blank" href="https://engineering-shiny.org/index.html">📖️ Read</a>]<hr/></div>
<div class="abook gis"><span style="font-size:2em"> 📔️ </span><b>Essentials of Geographic Information Systems</b> [<a target="_blank" href="https://saylordotorg.github.io/text_essentials-of-geographic-information-systems/index.html">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis stats ml rlang python"><span style="font-size:2em"> 📔️ </span><b>Explanatory Model Analysis</b> - Biecek & Burzykowski [<a target="_blank" href="https://ema.drwhy.ai/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis stats ml rlang"><span style="font-size:2em"> 📔️ </span><b>Exploratory Data Analysis with R</b> - Roger D. Peng [<a target="_blank" href="https://bookdown.org/rdpeng/exdata/">📖️ Read</a>]<hr/></div>
<div class="abook db rlang"><span style="font-size:2em"> 📔️ </span><b>Exploring Enterprise Databases with R: A Tidyverse Approach</b> - Smith, Yang, et al [<a target="_blank" href="https://smithjd.github.io/sql-pet/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis rlang vis bigdata"><span style="font-size:2em"> 📔️ </span><b>Exploring, Visualizing, and Modeling Big Data with R</b> - Bulut & Desjardins [<a target="_blank" href="https://okanbulut.github.io/bigdata/">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>Feature Engineering and Selection</b> - Kuhn & Johnson [<a target="_blank" href="http://www.feat.engineering/">📖️ Read</a>]<hr/></div>
<div class="abook fin datsci"><span style="font-size:2em"> 📔️ </span><b>Financial Data Science</b> - Riordan [<a target="_blank" href="https://bookdown.org/lisakmnsk/LMU_FINTECH_financial_data_science/">📖️ Read</a>]<hr/></div>
<div class="abook fin rlang regression dataanalysis ml"><span style="font-size:2em"> 📔️ </span><b>Forecasting: Principles and Practice</b> - Hyndman & Athanasopoulos [<a target="_blank" href="https://otexts.com/fpp3/">📖️ Read</a>]<hr/></div>
<div class="abook vis"><span style="font-size:2em"> 📔️ </span><b>Fundamentals of Data Visualization</b> - Claus O. Wilke [<a target="_blank" href="https://clauswilke.com/dataviz/">📖️ Read</a>]<hr/></div>
<div class="abook gis python"><span style="font-size:2em"> 📔️ </span><b>Geocomputation with Python</b> - Dorman, Graser, et al [<a target="_blank" href="https://py.geocompx.org">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Geocomputation with R</b> - Lovelace, Nowosad, & Muenchow [<a target="_blank" href="https://r.geocompx.org">📖️ Read</a>]<hr/></div>
<div class="abook gis dataanalysis"><span style="font-size:2em"> 📔️ </span><b>Geographic Data Analysis</b> - University of Oregon [<a target="_blank" href="https://pjbartlein.github.io/GeogDataAnalysis/index.html">📖️ Read</a>]<hr/></div>
<div class="abook gis python"><span style="font-size:2em"> 📔️ </span><b>Geographic Data Science with Python</b> - Rey, Arribas-Bel, Wolf [<a target="_blank" href="https://geographicdata.science/book/intro">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Geographic Data Science with R</b> - Wimberly [<a target="_blank" href="https://bookdown.org/mcwimberly/gdswr-book/">📖️ Read</a>]<hr/></div>

<div class="abook gis rlang bio"><span style="font-size:2em"> 📔️ </span><b>Geospatial Health Data: Modeling and Visualization with R-INLA and Shiny</b> - Moraga, P [<a target="_blank" href="https://www.paulamoraga.com/book-geospatial/">📖️ Read</a>]<hr/></div>

<div class="abook vis rlang dataanalysis"><span style="font-size:2em"> 📔️ </span><b>ggplot2: elegant graphics for data analysis</b> - Wickham, Navarro, & Pedersen [<a target="_blank" href="https://ggplot2-book.org/">📖️ Read</a>]<hr/></div>
<div class="abook vis "><span style="font-size:2em"> 📔️ </span><b>Hands-On Data Visualization</b> - Dougherty & Ilyankou [<a target="_blank" href="https://handsondataviz.org/">📖️ Read</a>]<hr/></div>
<div class="abook ml rlang"><span style="font-size:2em"> 📔️ </span><b>Hands-On Machine Learning with R</b> - Boehmke & Greenwell [<a target="_blank" href="https://bradleyboehmke.github.io/HOML/">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>Hands-On Programming with R</b> - Garrett Grolemund [<a target="_blank" href="https://rstudio-education.github.io/hopr/">📖️ Read</a>]<hr/></div>
<div class="abook devops rlang"><span style="font-size:2em"> 📔️ </span><b>Happy Git and GitHub for the useR</b> - Jennifer Bryan [<a target="_blank" href="https://happygitwithr.com/index.html">📖️ Read</a>]<hr/></div>
<div class="abook ml"><span style="font-size:2em"> 📔️ </span><b>Interpretable Machine Learning</b> - Christoph Molnar [<a target="_blank" href="https://christophm.github.io/interpretable-ml-book/">📖️ Read</a>]<hr/></div>
<div class="abook math"><span style="font-size:2em"> 📔️ </span><b>Introduction to Applied Linear Algebra</b> - Boyd & Vandenberghe [<a target="_blank" href="https://web.stanford.edu/~boyd/vmls/">📖️ Read</a>]<hr/></div>
<div class="abook hum analytics python"><span style="font-size:2em"> 📔️ </span><b>Introduction to Cultural Analytics & Python</b> - Melanie Walsh [<a target="_blank" href="https://melaniewalsh.github.io/Intro-Cultural-Analytics/welcome.html">📖️ Read</a>]<hr/></div>
<div class="abook datsci dataanalysis ml"><span style="font-size:2em"> 📔️ </span><b>Introduction to Data Science: Data Analysis and Prediction Algorithms with R </b> - Irizarry [<a target="_blank" href="http://rafalab.dfci.harvard.edu/dsbook-part-1/">📖️ Read</a>]<hr/></div>
<div class="abook gis datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Introduction to Environmental Data Science</b> - Jerry Davis [<a target="_blank" href="https://bookdown.org/igisc/EnvDataSci/">📖️ Read</a>]<hr/></div>
<div class="abook gis"><span style="font-size:2em"> 📔️ </span><b>Introduction to GIS</b> - Víctor Olaya [<a target="_blank" href="https://volaya.github.io/gis-book/en/index.html">📖️ Read</a>]<hr/></div>
<div class="abook inference stats"><span style="font-size:2em"> 📔️ </span><b>Introduction to Inferential Statistics</b> - Trussler, Radley [<a target="_blank" href="https://bookdown.org/marc_trussler/IIS/">📖️ Read</a>]<hr/></div>
<div class="abook stats inference"><span style="font-size:2em"> 📔️ </span><b>Introduction to Modern Statistics</b> - Çetinkaya-Rundel & Hardin [<a target="_blank" href="https://openintro-ims.netlify.app/index.html">📖️ Read</a>]<hr/></div>
<div class="abook prob datsci"><span style="font-size:2em"> 📔️ </span><b>Introduction to Probability for Data Science</b> - Stanley H. Chan [<a target="_blank" href="https://probability4datascience.com/index.html">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Introduction to Spatial Data Programming with R</b> - Dorman [<a target="_blank" href="https://geobgu.xyz/r/">📖️ Read</a>]<hr/></div>

<div class="abook prob stats datsci"><span style="font-size:2em"> 📔️ </span><b>Introduction to Statistics and Data Science</b> - Northwestern [<a target="_blank" href="https://nustat.github.io/intro-stat-ds/">📖️ Read</a>]<hr/></div>

<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>Introductory Statistics</b> - Saylar Academy [<a target="_blank" href="https://saylordotorg.github.io/text_introductory-statistics/index.html">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Intro to GIS and Spatial Analysis</b> - Manuel Gimond [<a target="_blank" href="https://mgimond.github.io/Spatial/">📖️ Read</a>]<hr/></div>
<div class="abook python"><span style="font-size:2em"> 📔️ </span><b>IPython Cookbook 2ed</b> - Cyrille Rossant [<a target="_blank" href="https://ipython-books.github.io/">📖️ Read</a>]<hr/></div>
<div class="abook ml stats"><span style="font-size:2em"> 📔️ </span><b>ISLR2 Labs (as a book)</b> [<a target="_blank" href="https://datlinux.gitlab.io/islr2/">📖️ Read</a>]<hr/></div>
<div class="abook julia datsci"><span style="font-size:2em"> 📔️ </span><b>Julia Data Science</b> - Storopoli, Huijzer & Alonso [<a target="_blank" href="https://juliadatascience.io/">📖️ Read</a>]<hr/></div>
<div class="abook math jupyter"><span style="font-size:2em"> 📔️ </span><b>Jupyter Guide to Linear Algebra</b> - Vanderlei [<a target="_blank" href="https://bvanderlei.github.io/jupyter-guide-to-linear-algebra/intro.html">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>Learning Data Science</b> - Lau, Gonzalez, & Nolan [<a target="_blank" href="https://www.textbook.ds100.org/intro.html">📖️ Read</a>]<hr/></div>
<div class="abook ml jupyter math stats"><span style="font-size:2em"> 📔️ </span><b>Learning From Data</b> - Forssén  [<a target="_blank" href="https://cforssen.gitlab.io/tif285-book/content/Intro/welcome.html">📖️ Read</a>]<hr/></div>
<div class="abook stats rlang hum"><span style="font-size:2em"> 📔️ </span><b>Learning statistics with R: A tutorial for psychology students and other beginners</b> - Navarro [<a target="_blank" href="https://learningstatisticswithr.com/book/">📖️ Read</a>]<hr/></div>
<div class="abook math datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Linear Algebra for Data Science with examples in R</b> - Shaina Race Bennett [<a target="_blank" href="https://shainarace.github.io/LinearAlgebra/">📖️ Read</a>]<hr/></div>
<div class="abook math"><span style="font-size:2em"> 📔️ </span><b>Linear Algebra</b> - WikiBooks [<a target="_blank" href="https://en.wikibooks.org/wiki/Linear_Algebra">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci ml"><span style="font-size:2em"> 📔️ </span><b>Little Book of R for Time Series</b> - Coghlan [<a target="_blank" href="https://a-little-book-of-r-for-time-series.readthedocs.io/en/latest/">📖️ Read</a>]<hr/></div>
<div class="abook ml bigdata"><span style="font-size:2em"> 📔️ </span><b>Machine Learning and Big Data</b> - Alkaseer [<a target="_blank" href="https://www.kareemalkaseer.com/books/ml">📖️ Read</a>]<hr/></div>
<div class="abook ml"><span style="font-size:2em"> 📔️ </span><b>Machine Learning for Data Streams</b> - Bifet, Gavaldà, et al. [<a target="_blank" href="https://book.moa.cms.waikato.ac.nz/title_page.html/">📖️ Read</a>]<hr/></div>
<div class="abook python datsci math"><span style="font-size:2em"> 📔️ </span><b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> - DAT Linux [<a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/">📖️ Read</a>]<hr/></div>
<div class="abook apps rlang"><span style="font-size:2em"> 📔️ </span><b>Mastering Shiny</b> - Wickham [<a target="_blank" href="https://mastering-shiny.org/index.html">📖️ Read</a>]<hr/></div>
<div class="abook apps rlang"><span style="font-size:2em"> 📔️ </span><b>Mastering Software Development in R</b> - Peng, Kross [<a target="_blank" href="https://bookdown.org/rdpeng/RProgDA/">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci"><span style="font-size:2em"> 📔️ </span><b>Mastering Spark with R</b> - Luraschi, Kuo, & Ruiz [<a target="_blank" href="https://therinspark.com/">📖️ Read</a>]<hr/></div>
<div class="abook mining bigdata"><span style="font-size:2em"> 📔️ </span><b>Mining of Massive Datasets</b> - Leskovec, Rajaraman, Ullman [<a target="_blank" href="http://www.mmds.org/make Neural">📖️ Read</a>]<hr/></div>
<div class="abook ml rlang"><span style="font-size:2em"> 📔️ </span><b>Applied Machine Learning Using mlr3 in R</b> - Becker, Binder, et al [<a target="_blank" href="https://mlr3book.mlr-org.com/">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis"><span style="font-size:2em"> 📔️ </span><b>Modern Applied Data Analysis</b> - University of Georgia [<a target="_blank" href="https://andreashandel.github.io/MADAcourse/">📖️ Read</a>]<hr/></div>
<div class="abook datsci rlang"><span style="font-size:2em"> 📔️ </span><b>Modern Data Science with R: 3ed</b> - Baumer, Kaplan, & Horton [<a target="_blank" href="https://mdsr-book.github.io/mdsr3e/">📖️ Read</a>]<hr/></div>
<div class="abook rlang vis"><span style="font-size:2em"> 📔️ </span><b>Modern Data Visualization with R</b> - Kabacoff [<a target="_blank" href="https://rkabacoff.github.io/datavis/">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci"><span style="font-size:2em"> 📔️ </span><b>Modern R with the tidyverse</b> - Rodrigues [<a target="_blank" href="http://modern-rstats.eu/">📖️ Read</a>]<hr/></div>
<div class="abook rlang stats"><span style="font-size:2em"> 📔️ </span><b>Modern Statistics with R</b> - Måns Thulin [<a target="_blank" href="https://www.modernstatisticswithr.com/">📖️ Read</a>]<hr/></div>
<div class="abook nlp python datsci"><span style="font-size:2em"> 📔️ </span><b>Natural Language Processing with Python</b> - Bird, Klein, Loper [<a target="_blank" href="https://www.nltk.org/book/">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>Network Data Science</b> - Pedigo [<a target="_blank" href="https://bdpedigo.github.io/networks-course/landing.html">📖️ Read</a>]<hr/></div>
<div class="abook deep ml"><span style="font-size:2em"> 📔️ </span><b>Neural Networks and Deep Learning</b> - Nielson [<a target="_blank" href="http://neuralnetworksanddeeplearning.com/">📖️ Read</a>]<hr/></div>
<div class="abook ml nlp"><span style="font-size:2em"> 📔️ </span><b>Pattern Recognition and Machine Learning</b> - Bishop [<a target="_blank" href="https://www.microsoft.com/en-us/research/publication/pattern-recognition-machine-learning/">📖️ Read</a>]<hr/></div>
<div class="abook datsci practice"><span style="font-size:2em"> 📔️ </span><b>Practical Data Science</b> - Clark [<a target="_blank" href="https://m-clark.github.io/data-processing-and-visualization/">📖️ Read</a>]<hr/></div>
<div class="abook stats prob"><span style="font-size:2em"> 📔️ </span><b>Probabilistic Programming: Bayesian Methods for Hackers</b> - Davidson-Pilon [<a target="_blank" href="https://dataorigami.net/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/">📖️ Read</a>]<hr/></div>
<div class="abook prob stats datsci"><span style="font-size:2em"> 📔️ </span><b>Probability, Statistics, and Data: A Fresh Approach Using R</b> - Speegle & Clair [<a target="_blank" href="https://mathstat.slu.edu/~speegle/_book/preface.html">📖️ Read</a>]<hr/></div>
<div class="abook devops"><span style="font-size:2em"> 📔️ </span><b>Pro Git</b> - Chacon & Straub [<a target="_blank" href="https://git-scm.com/book">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>PSPP for Beginners</b> [<a target="_blank" href="https://www.garyfisk.com/pspp/index.html">📖️ Read</a>]<hr/></div>
<div class="abook python datsci"><span style="font-size:2em"> 📔️ </span><b>Python Data Science Handbook</b> - Jake VanderPlas [<a target="_blank" href="https://jakevdp.github.io/PythonDataScienceHandbook/">📖️ Read</a>]<hr/></div>
<div class="abook python dataanalysis"><span style="font-size:2em"> 📔️ </span><b>Python for Data Analysis, 3E</b> - Wes McKinney  [<a target="_blank" href="https://wesmckinney.com/book/">📖️ Read</a>]<hr/></div>
<div class="abook qgis"><span style="font-size:2em"> 📔️ </span><b>QGIS for Transport Research</b> - Lovelace, Morgan, & Philips [<a target="_blank" href="https://itsleeds.github.io/QGIS-intro/">📖️ Read</a>]<hr/></div>
<div class="abook stats rlang dataanalysis ml"><span style="font-size:2em"> 📔️ </span><b>R Companion to Statistics: Data Analysis and Modelling</b> - Maarten Speekenbrink [<a target="_blank" href="https://mspeekenbrink.github.io/sdam-r-companion/">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci"><span style="font-size:2em"> 📔️ </span><b>R Cookbook</b> - Long & Teetor [<a target="_blank" href="https://rc2e.com/">📖️ Read</a>]<hr/></div>
<div class="abook stats regression"><span style="font-size:2em"> 📔️ </span><b>Regression and Other Stories</b> - Gelman, Hill, & Vehtari [<a target="_blank" href="https://avehtari.github.io/ROS-Examples/">📖️ Read</a>]<hr/></div>
<div class="abook ml fin practice"><span style="font-size:2em"> 📔️ </span><b>Reproducible Machine Learning for Credit Card Fraud detection: Practical handbook</b> - Borgne, Siblini , et al [<a target="_blank" href="https://fraud-detection-handbook.github.io/fraud-detection-handbook/Foreword.html">📖️ Read</a>]<hr/></div>
<div class="abook rlang dataanalysis"><span style="font-size:2em"> 📔️ </span><b>R for Data Analysis</b> - French [<a target="_blank" href="https://trevorfrench.github.io/R-for-Data-Analysis/">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci"><span style="font-size:2em"> 📔️ </span><b>R for Data Science</b> - Wickham & Grolemund [<a target="_blank" href="https://r4ds.hadley.nz/">📖️ Read</a>]<hr/></div>
<div class="abook rlang gis"><span style="font-size:2em"> 📔️ </span><b>R for Geographic Data Science</b> - Stefano De Sabbata [<a target="_blank" href="https://sdesabbata.github.io/r-for-geographic-data-science/">📖️ Read</a>]<hr/></div>
<div class="abook rlang hum"><span style="font-size:2em"> 📔️ </span><b>R for Health Data Science</b> - Harrison, Pius [<a target="_blank" href="https://argoshare.is.ed.ac.uk/healthyr_book/">📖️ Read</a>]<hr/></div>
<div class="abook rlang vis"><span style="font-size:2em"> 📔️ </span><b>R Graphics Cookbook 2ed</b> - Winston Chang [<a target="_blank" href="https://r-graphics.org/">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>R Markdown: The Definitive Guide</b> - Xie, Allaire, & Grolemund [<a target="_blank" href="https://bookdown.org/yihui/rmarkdown/">📖️ Read</a>]<hr/></div>
<div class="abook rlang datsci"><span style="font-size:2em"> 📔️ </span><b>R Programming for Data Science</b> - Roger D. Peng [<a target="_blank" href="https://bookdown.org/rdpeng/rprogdatascience/">📖️ Read</a>]<hr/></div>
<div class="abook rkang"><span style="font-size:2em"> 📔️ </span><b>R Programming</b> - WikiBooks [<a target="_blank" href="https://en.wikibooks.org/wiki/R_Programming">📖️ Read</a>]<hr/></div>
<div class="abook rlang"><span style="font-size:2em"> 📔️ </span><b>R Programming: Zero to Pro</b> - Feng & Zhu [<a target="_blank" href="https://r02pro.github.io/">📖️ Read</a>]<hr/></div>
<div class="abook python datsci"><span style="font-size:2em"> 📔️ </span><b>Scipy Lecture Notes</b> - scipy-lectures.org [<a target="_blank" href="https://scipy-lectures.org/">📖️ Read</a>]<hr/></div>
<div class="abook vis"><span style="font-size:2em"> 📔️ </span><b>Seeing Theory</b> [<a target="_blank" href="https://drfloreiche.github.io/index.html">📖️ Read</a>]<hr/></div>
<div class="abook hum mining"><span style="font-size:2em"> 📔️ </span><b>Social Media Mining: An Introduction</b> - Zafarani, Abbasi, Liu [<a target="_blank" href="http://www.socialmediamining.info/book/">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Spatial Data Science</b> - Pebesma & Bivand [<a target="_blank" href="https://r-spatial.org/book/">📖️ Read</a>]<hr/></div>
<div class="abook gis rlang"><span style="font-size:2em"> 📔️ </span><b>Spatial Modelling for Data Scientists</b> - Rowe & Arribas-Bel [<a target="_blank" href="https://gdsl-ul.github.io/san/">📖️ Read</a>]<hr/></div>
<div class="abook gis stats rlang"><span style="font-size:2em"> 📔️ </span><b>Spatial Statistics for Data Science: Theory and Practice with R</b> - Moraga [<a target="_blank" href="https://www.paulamoraga.com/book-spatial/index.html">📖️ Read</a>]<hr/></div>
<div class="abook nlp ml"><span style="font-size:2em"> 📔️ </span><b>Speech and Language Processing</b> - Jurafsky & Martin [<a target="_blank" href="https://web.stanford.edu/~jurafsky/slp3/">📖️ Read</a>]<hr/></div>
<div class="abook stats prob"><span style="font-size:2em"> 📔️ </span><b>STA237: Probability, Statistics and Data Analysis I</b> - University of Toronto [<a target="_blank" href="https://mjmoon.gitlab.io/sta237/notes/">📖️ Read</a>]<hr/></div>
<div class="abook stats prob"><span style="font-size:2em"> 📔️ </span><b>STAT 414: Introduction to Probability Theory</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat414/">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>STAT 500: Applied Statistics</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat500/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml regression"><span style="font-size:2em"> 📔️ </span><b>STAT 501: Regression Methods</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat501/">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>STAT 505: Applied Multivariate Statistical Analysis</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat505/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml mining"><span style="font-size:2em"> 📔️ </span><b>STAT 508: Applied Data Mining and Statistical Learning</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat508/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml"><span style="font-size:2em"> 📔️ </span><b>STAT 510: Applied Time Series Analysis</b> - Penn. State [<a target="_blank" href="https://online.stat.psu.edu/stat510/">📖️ Read</a>]<hr/></div>
<div class="abook stats datsci rlang"><span style="font-size:2em"> 📔️ </span><b>STAT 545: Data wrangling, exploration, and analysis with R</b> - Bryan [<a target="_blank" href="https://stat545.com/">📖️ Read</a>]<hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>Statistical Analysis Handbook</b> - Dr Michael J de Smith [<a target="_blank" href="https://www.statsref.com/HTML/index.html">📖️ Read</a>]<hr/></div>
<div class="abook stats inference"><span style="font-size:2em"> 📔️ </span><b>Statistical Inference via Data Science</b> - Ismay & Kim [<a target="_blank" href="https://moderndive.com/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml"><span style="font-size:2em"> 📔️ </span><b>Statistical Learning with Sparsity: The Lasso and Generalizations</b> - Hastie, Tibshirani, Wainwright [<a target="_blank" href="https://hastie.su.domains/StatLearnSparsity/">📖️ Read</a>]<hr/></div>
<div class="abook stats datsci ml"><span style="font-size:2em"> 📔️ </span><b>Statistical Methods for Data Science</b> - Elizabeth Purdom [<a target="_blank" href="https://epurdom.github.io/Stat131A/book/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml"><span style="font-size:2em"> 📔️ </span><b>Statistical Thinking for the 21st Century</b> - Russell A. Poldrack [<a target="_blank" href="https://statsthinking21.github.io/statsthinking21-core-site/index.html">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>Advanced Data Science</b> - Irizarry [<a target="_blank" href="http://rafalab.dfci.harvard.edu/dsbook-part-2/">📖️ Read</a>]<hr/></div>
<div class="abook stats ml"><span style="font-size:2em"> 📔️ </span><b>Statistics: Data analysis and modelling</b> - Maarten Speekenbrink [<a target="_blank" href="https://mspeekenbrink.github.io/sdam-book/">📖️ Read</a>]<hr/></div>
<div class="abook ml nlp"><span style="font-size:2em"> 📔️ </span><b>Supervised Machine Learning for Text Analysis in R</b> - Hvitfeldt & Silge [<a target="_blank" href="https://smltar.com/">📖️ Read</a>]<hr/></div>
<div class="abook vis rlang python"><span style="font-size:2em"> 📔️ </span><b>Telling Stories with Data: With Applications in R and Python</b> - Alexander [<a target="_blank" href="https://tellingstorieswithdata.com/">📖️ Read</a>]<hr/></div>
<div class="abook nlp rlang"><span style="font-size:2em"> 📔️ </span><b>Text Analysis with R</b> - Engel & Bailey [<a target="_blank" href="https://cengel.github.io/R-text-analysis/">📖️ Read</a>]<hr/></div>
<div class="abook nlp mining hum"><span style="font-size:2em"> 📔️ </span><b>Text Mining for Social Scientists</b> - Lennert [<a target="_blank" href="https://bookdown.org/f_lennert/text-mining-book/">📖️ Read</a>]<hr/></div>
<div class="abook rlang nlp mining"><span style="font-size:2em"> 📔️ </span><b>Text Mining with R</b> - Silge & Robinson [<a target="_blank" href="https://www.tidytextmining.com/">📖️ Read</a>]<hr/></div>
<div class="abook rlang nlp mining"><span style="font-size:2em"> 📔️ </span><b>Text Mining with Tidy Data Principles</b> - Silge [<a target="_blank" href="https://juliasilge.shinyapps.io/learntidytext/">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>The Book of OHDSI</b> [<a target="_blank" href="https://ohdsi.github.io/TheBookOfOhdsi/">📖️ Read</a>]<hr/></div>
<div class="abook datsci practice"><span style="font-size:2em"> 📔️ </span><b>The Data Engineering Cookbook</b> -  Andreas Kretz [<a target="_blank" href="">📖️ Read</a>]<hr/></div>
<div class="abook practice datsci"><span style="font-size:2em"> 📔️ </span><b>The Data Science Interview Book</b> [<a target="_blank" href="https://book.thedatascienceinterviewproject.com/">📖️ Read</a>]<hr/></div>
<div class="abook datsci"><span style="font-size:2em"> 📔️ </span><b>The Data Validation Cookbook</b> - van der Loo & ten Bosch [<a target="_blank" href="https://data-cleaning.github.io/validate/">📖️ Read</a>]<hr/></div>
<div class="abook python"><span style="font-size:2em"> 📔️ </span><b>The Hitchhiker's Guide to Python</b> - Reitz & Schlusser [<a target="_blank" href="https://docs.python-guide.org/">📖️ Read</a>]<hr/></div>
<div class="abook datsci practice"><span style="font-size:2em"> 📔️ </span><b>The Turing Way: Handbook to reproducible, ethical and collaborative data science</b> - Arnold, Bowler et al [<a target="_blank" href="https://the-turing-way.netlify.app">📖️ Read</a>] <hr/></div>
<div class="abook stats"><span style="font-size:2em"> 📔️ </span><b>Think Bayes 2</b> - Allen B. Downey [<a target="_blank" href="https://allendowney.github.io/ThinkBayes2/index.html">📖️ Read</a>]<hr/></div>
<div class="abook stats python"><span style="font-size:2em"> 📔️ </span><b>Think Stats: Exploratory Data Analysis in Python</b> - Allen B. Downey [<a target="_blank" href="https://greenteapress.com/thinkstats2/html/index.html">📖️ Read</a>]<hr/></div>
<div class="abook dataanalysis rlang"><span style="font-size:2em"> 📔️ </span><b>Exploratory Data Analysis in R</b> - Gimond [<a target="_blank" href="https://mgimond.github.io/ES218/">📖️ Read</a>]<hr/></div>
<div class="abook bi vis"><span style="font-size:2em"> 📔️ </span><b>Introduction to Power BI</b> - Monash Bioinformatics Platform [<a target="_blank" href="https://monashdatafluency.github.io/Power_BI/index.html">📖️ Read</a>]<hr/></div>
<div class="abook bi vis"><span style="font-size:2em"> 📔️ </span><b>Learning Data Visualization with Tableau</b> - Hayley Boyce [<a target="_blank" href="https://hfboyce.github.io/tableau_course/intro.html">📖️ Read</a>]<hr/></div>

<br/><br/><br/>

{{< /rawhtml >}}

### Other ways to show your support:

{{< rawhtml >}}
<table>
<tr style="background-color:#FFCCCC">
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license. <a href="/pro">Get PRO now!</a>
</td>
</tr>
</table>
<br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
