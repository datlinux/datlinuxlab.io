---
title: Download
subtitle: ⬇️ Download the DAT Linux ISO
comments: false
---

#####

 {{< rawhtml >}}
<table style=margin-top:20px;margin-bottom:20px;>
<tr style="background-color:#FFCCCC">
<td style="width:90px;height:116px;padding:0px;background-color:#e4e4e4;font-size:1.3em;text-align:center;">
    <a href="/pro"> 🧰️ <br/> PRO </a>
</td>
<td style="padding:5px;color:#333333;">
    Introducing: DAT Linux <a href="/pro">PRO tools</a>. Enhance your DAT Linux with extra power-tools including back-up/restore, app update notifications, app monitoring, custom links tab, dark theme, etc. One payment, perpetual license.<br/><a href="/pro">Get PRO now! (🎓 Student & 🏫 Educator discounts!!)</a>
</td>
</tr>
</table>
{{< /rawhtml >}}

___ 

### Download version 2.0

DAT Linux 2.0 is a major release (based on Lubuntu 24.04.1).

{{< rawhtml >}}
<div>	
<a style="float:left" target="_blank" href="https://sourceforge.net/projects/dat-linux/files/">
	<img alt="Download DAT Linux" src="https://a.fsdn.com/con/app/sf-download-button" 
	 width=276 height=48 srcset="https://a.fsdn.com/con/app/sf-download-button?button_size=2x 2x">
</a>
</div>
<br/>
<br/>
{{< /rawhtml >}}

___ 

- `MD5:   963d8533af845b696087bb0f7807737e`
- `SHA1:  e82973b95754366504f3080d592642ea2d911157`

___ 

### Installation system specs

- x86_64 CPU
- Min. 3GB RAM
- Min. 20GB disk space

___ 

### Other ways to show your support:

{{< rawhtml >}}
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:90px;padding:5px;background-color:#e4e4e4;">
    <img style="width:90px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:5px;color:#333333;">
    <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team. <a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/"> 
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
