---
title: PRO
subtitle: 🧰️ DAT Linux PRO tools
comments: false
---

___

### GET PRO

{{< rawhtml >}}
<br/>
<a target="_blank" href="https://www.paypal.com/ncp/payment/9TPAQH4TK3GXL">
🧰 PAYPAL (single user license $39 AUD)</a> (Buy up to 2)
<br/><br/>
<a target="_blank" href="https://www.paypal.com/ncp/payment/N8TFQAUUXNQYA">
🧰️ PAYPAL (3 user licenses $109 AUD)</a> (~$36 per license; buy up to 3 sets of 3)
<br/><br/>
<a target="_blank" href="https://www.paypal.com/ncp/payment/AAHEWB6LKUEAQ">
🧰️ PAYPAL (10 user licenses $320 AUD)</a> (~$33 per license; buy up to 10 sets of 10)
<br/><br/>
{{< /rawhtml >}}

After payment the license(s) will be created, and you will be sent your client key(s) via email. Please allow up to 24 hours for processing.

___

### ABOUT PRO

#### 

Enhance your DAT Linux experience with extra power-tools including: back-up & restore; 
app upgrade notifications; app monitoring; custom links tab; dark theme; priority support;
e-books assistant; dataset finder; all future PRO upgrades incl. new tools (on the DAT Linux 2.x release stream supported until 2029). 
One payment, perpetual license. All for a low price of only $AUD**39.00** via Paypal, with volume discounts.

> ATTENTION **🎓 STUDENTS** & **🏫 EDUCATORS**: Send an email to `pro` at `datlinux` dot `com` from your personal school/college/university 
email and get PRO for only $AUD**12** (student) or $AUD**24** (educator)! *(\*Paypal only. Payment link to be provided on receipt of email.)*

{{< gallery dir="/album-pro/" caption-effect="fade" />}}

- 🗣️ Every PRO user gets their name/business and website/social listed on the datlinux.com website.

___ 

### INSTALL PRO

Make sure to upgrade your system to get the latest DAT Linux packages:

`sudo apt-get update`

`sudo apt-get upgrade`

Then run the "Install Pro" tool from the "PRO Tools" tab in the DAT Linux Control Panel. 

You will be asked to enter the customer id (email) and secret.

___ 

### QUESTIONS?

Please contact `pro` AT `datlinux` DOT `com` for any enquiries re PRO tools.

___ 

### PRO MENTIONS

A big thanks to PRO trial participants, and new PRO users..

{{< rawhtml >}}
Damon Lehr: <a href="" target="_blank"></a><br/>
Stephen Hinkley: <a href="https://www.instagram.com/slhink98/" target="_blank">instagram.com/slhink98</a><br/>
Leon Sugden: <a href="https://twitter.com/leonsri52" target="_blank">twitter.com/leonsri52</a><br/>
K. Corcoran: <a href="https://profiles.stanford.edu/kate-corcoran" target="_blank">profiles.stanford.edu/kate-corcoran</a><br/>
Josh, Almonte: <a href="" target="_blank"></a><br/>
L Averyt: <a href="https://www.facebook.com/len.averyt/" target="_blank">facebook.com/len.averyt</a><br/>
Frank (Cologne): <a href="https://www.toptal.com/resume/frank-buss" target="_blank">toptal.com/resume/frank-buss</a><br/>
James Davis: <a href="" target="_blank"></a><br/>
DExUSA: <a href="" target="_blank"></a><br/>
Matt K: <a href="https://www.researchgate.net/scientific-contributions/Matt-Krupp-2066508410" target="_blank">researchgate.net/scientific-contributions/Matt-Krupp-2066508410</a><br/>
Derek Mazer: <a href="" target="_blank"></a><br/>
Martijn Buikema (Netherlands): <a href="https://nl.linkedin.com/in/martijnbuikema" target="_blank">nl.linkedin.com/in/martijnbuikema</a><br/>
Dave Holloway
{{< /rawhtml >}}

___ 

### Other ways to show your support:

{{< rawhtml >}}
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:90px;padding:5px;background-color:#e4e4e4;">
    <img style="width:90px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:5px;color:#333333;">
    <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team. <a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/"> 
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
