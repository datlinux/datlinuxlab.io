---
title: PRO
subtitle: 🧰️ Thank you 
comments: false
---

___

### PAYMENT

#### 

Congratulations on your PRO subscription purchase. We'll process the request and send the license key(s) via email within 24 hours.

Once you have the licence key, see the the [main PRO page](/pro#install-pro) for installation instructions.

___ 

>If you'd like a link in the "Mentions" section of the PRO page, send an email to `pro` at `datlinux` dot `com` with the following:
> 
> - Visible name or business/insitution name
> - Contact link - email adddress, website URL, or social media page
> - Your location (optional)


___ 

### Other ways to show your support:

{{< rawhtml >}}
<table style="width:100%;">
<tr style="background-color:#FFFF99">
<td style="width:90px;padding:5px;background-color:#e4e4e4;">
    <img style="width:90px;padding:0" src="/numpybyexample.png" />
</td>
<td style="padding:5px;color:#333333;">
    <b>NUMPY BY EXAMPLE - A Beginner's Guide to Learning NumPy</b> by the DAT Linux team. <a target="_blank" href="https://datlinux.com/numpybyexample/_build/html/"> 
    <br/><a target="_blank" href="https://leanpub.com/numpybyexample">🛒️ BUY the PDF or EPUB e-book from Leanpub</a>.
</td>
</tr>
</table><br/>
{{< /rawhtml >}}

> 💳️ [Please subscribe/donate to help support DAT Linux development](/donate)
