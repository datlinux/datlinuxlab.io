---
title: FAQ
subtitle: ℹ️ Answers to some questions you may have
comments: false
editor_options: 
  markdown: 
    wrap: 72
---

------------------------------------------------------------------------

### FAQ for release: *DAT Linux 2.0*, Nov 2024

Get the PDF version: [https://datlinux.com/DAT Linux 2.0 FAQ.pdf](/DAT%20Linux%202.0%20FAQ.pdf)

------------------------------------------------------------------------

### What is DAT Linux?

⚠️ NOTE: DAT Linux comes with no guarantees or warranty.

------------------------------------------------------------------------

DAT Linux is a **Linux distribution for data science**. It brings
together all your favourite *open-source* data science tools and apps
into a ready-to-run desktop environment.

It's based on a customised Ubuntu (called a *re-spin*\*\*), further
improved with custom-built tools for supporting the easy management of
dozens of software apps and tools for data science.

\*\**Lubuntu 24.04 LTS*

------------------------------------------------------------------------

### What data science applications are supported?

------------------------------------------------------------------------

Please see the 📶️ **Home** page for a detailed run-down of supported
apps: [https://datlinux.com](/)

------------------------------------------------------------------------

### Who should use it?

------------------------------------------------------------------------

DAT Linux is perfect for students, professionals, academics, or anyone
interested in data science who doesn't want to spend endless hours
downloading, installing, configuring, and maintaining applications from
a range of sources, each with different technical requirements and
set-up challenges.

------------------------------------------------------------------------

### Who shouldn't use it?

------------------------------------------------------------------------

Linux generally, and Ubuntu/DAT-Linux specifically, is open and free to
use by anyone, without any restrictions.

If there are policies in a corporate setting with, say, specific
restrictions on users' computing environments, it can be run from a USB
boot device or virtualised environment.

Otherwise DAT Linux can be installed anywhere, on as many desktops as
needed.

- See the ⬇️ **Download** page for recommended system specs: [https://datlinux.com/download](/download)
- See the 💬️ **Support** page for getting help: [https://datlinux.com/support](/support)

------------------------------------------------------------------------

### What about support?

------------------------------------------------------------------------

The main DAT Linux Support landing page can be found here:
[https://datlinux.com/support](/support)

#### Community

Visit the DAT Linux **Github Community** channel to post any feedback,
issues, or general questions related to DAT Linux:
[https://github.com/dat-linux/community/discussions](https://github.com/dat-linux/community/discussions/)

------------------------------------------------------------------------

### How can I report issues?

------------------------------------------------------------------------

For now, please post any issues on the DAT Linux **Github Community**
channel:
[https://github.com/dat-linux/community/discussions](https://github.com/dat-linux/community/discussions/)

------------------------------------------------------------------------

### Does DAT Linux have multi-language support? 

------------------------------------------------------------------------

Yes, to an extent. DAT Linux related icons and tool-tips in the menus will be 
localised; as are the title, tabs, launcher buttons, and tool-tips on the control 
panel. Launcher scripts (such as when installing or running a script-oriented 
DAT Linux app) are so far only in (British) English.

In terms of the OS generally, you should find most basic language packs are
available. The easiest way to check is switch your locale (in the "LXQT 
Configuration Settings" tool), re-login, and try it out.

Otherwise, to check for a language pack, run: 
- `check-language-support -l de` (eg, for German)
or 
- `check-language-support -l de_CH` (eg, for Swiss German)

If required, install it using: 
- `sudo apt -y install $(check-language-support -l de)`

Language/locale support for individual GUI programs (DAT Linux managed or 
otherwise) may vary, so please consult an app's documentation and follow any 
guidelines.

------------------------------------------------------------------------

### What does 'DAT' stand for?

------------------------------------------------------------------------

Nothing. Other than being cool looking and sounding, and the first three
letters of 'data' in 'data science'. It's pronounced like 'cat', or you
could spell it out as d.a.t. - whatever works.

------------------------------------------------------------------------

### What about an upgrade pathway?

------------------------------------------------------------------------

DAT Linux 2.0 is based on Ubuntu 24.04 LTS (long term support). Upgrading
from DAT Linux 1.0.x (if it's still on 22.04) will require upgrading your 
system to 24.04 followed by updating the control panel and other potential
configurations. Instructions for this will be provided on the support forum.

------------------------------------------------------------------------

### What systems is DAT Linux available on?

------------------------------------------------------------------------

Currently, only **x86_64** for desktop.

------------------------------------------------------------------------

### What's the DAT Linux Control Panel?

------------------------------------------------------------------------

This is the user interface to the ubiquitous DAT Linux installer (which
takes care of juggling a variety of installation methods), as well as
providing easy access to all the apps, help, and management tools.

Note that there are also shortcuts in the main menu for accessing all
apps & tools, without the need to open the control panel.

------------------------------------------------------------------------

### Why no Anaconda?

------------------------------------------------------------------------

Anaconda (aka conda/miniconda) did not meet a key goal of DAT Linux -
which is to provide ubiquitous and easy access to dozens of data science
apps, regardless of their origin.

Conda istelf is also bloated, and it doesn't have all the apps we wanted.
They're also very often not the latest (possible) releases - which
undermines another core tenet of DAT Linux.

------------------------------------------------------------------------

### Will more apps be added?

------------------------------------------------------------------------

This depends on demand and suitability. Have an idea for an app to
include? Then post it on **Github Community** channel:

[https://github.com/dat-linux/community/discussions](https://github.com/dat-linux/community/discussions/)

If or when new apps are added, these will show up as new icons or menu
items upon a Control Panel package (ie apt) upgrade.

------------------------------------------------------------------------

### How do I update or remove any supported data science applications?

------------------------------------------------------------------------

Open the "DAT Uninstaller" or the "DAT Updater" support apps, as
necessary. Then follow the prompts.

------------------------------------------------------------------------

### What happens to configuration or application settings after uninstall?

------------------------------------------------------------------------

That depends on the app. Some apps will leave settings files in your
home directory, and their settings *may* be restored when you reinstall
the app.

If this is important, please do your own research and backup before
uninstalling, as whilst it won't intentionally remove them, DAT Linux
will not actively preserve your custom settings or data for apps.

------------------------------------------------------------------------

### What happens if a DAT Linux app notifies me of a software update?

------------------------------------------------------------------------

Please do not update any supported apps independent of DAT Linux. The
DAT Updater will more than likely have noticed a software update, so use
it instead.

------------------------------------------------------------------------

### What about operating system updates?

------------------------------------------------------------------------

System updates are perfectly safe to perform (you may see a desktop
notification, or update options via a package manager application).

The *dat-linux-ctrl-pnl* package may be updated as part of a system
upgrade, and this is safe to do. It will keep the DAT Linux management
tools up to date, without affecting your apps and settings.

------------------------------------------------------------------------

### Is auto-update / notification available?

------------------------------------------------------------------------

Not yet, but this feature is being considered for a future upgrade.

------------------------------------------------------------------------

### Can I run multiple versions of the same app?

------------------------------------------------------------------------

This is potentially possible in the future, but not currently a feature
via the control panel\*. When you first set-up an app it will install
the latest version at that time. Updating will upgrade to the candidate
(latest) version, but remove the version it replaced.

\**If this is essential please go to the Github Community channel and
request instructions and advice on how to do this at the command line.
[https://github.com/dat-linux/community/discussions](https://github.com/dat-linux/community/discussions/)*

------------------------------------------------------------------------

### What is this 'System Info' desktop application thingy?

------------------------------------------------------------------------

If you know what a 'conky' is, then that's what this is. If you don't,
then it's just a 'sticky' desktop widget. 

The info panel gives you a snapshot view of your system and DAT Linux
apps and their versions (if installed).

------------------------------------------------------------------------

### The 'DAT System Info' desktop widget is hidden, how do I recover it?

------------------------------------------------------------------------

Check if it resides in another workspace.

Otherwise, run the 'DAT System Info' icon in the control panel ('DAT
Linux Admin' tab) or via the main menu / launcher, and it will reappear. 

It always acts as an on/off toggle.

------------------------------------------------------------------------

### Can I disable 'DAT System Info' from launching when I login?

------------------------------------------------------------------------

Sure, just delete (or backup) the file:
`~/.config/autostart/DAT_Conky.desktop`

To restore it, just restore the file:
`~/.config/autostart/DAT_Conky.desktop` from where you backed it up.

Alternatively, uninstall it using the "DAT Uninstaller" - it's named
'dconky'.

- Note, this app does not conflict with a standard 'conky' you may have installed separately. It's an independent binary and uses a non-standard configuration.

