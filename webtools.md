---
title: webtools
subtitle: ⚙️ webtools R package
comments: false
---

___ 

{{< rawhtml >}}
    <img src="/webtools.png" style="width:12%"/>
{{< /rawhtml >}}

### What is 'webtools'?
### 
An R package with a set of useful web tools to improve your productivity.

#### Installation:

`install.packages("https://l_m.gitlab.io/r-webtools-package/webtools_0.7.16.tar.gz")`

>DAT Linux comes with a convenient front end (D-Search) app for the dataset search functions.

#### 
#### Tools include: 
___

##### Searching DuckDuckGo from the console

`ddg('guid')`
>Get the first "instant result" or other matching results

##### 
##### Finding and load datasets across all CRAN packages (not just those you've installed)

`dsearch('Time series')`
>Search for packages (with datasets) matching the search term

`dinfo('TSrepr')`
>Get package/dataset info

`dfetch('TSrepr', 'elec_load.rda')`
>Fetch and load a dataset

##### 
##### Sharing a text file to a paste-bin

`sharebin()`
>Paste the current active file in RStudio and get a URL to share

`sharebin(file_name)`
>Choose a file to paste from the current working dir.

##### 
##### Getting a fast GUID

`guid()`
>Returns a GUID for immediate use

##### 
##### Useful info on all countries

`countries()`
>Get a dataframe of useful information on all countries such as FIFA codes, Dial codes, ISO codes, currency, official name, and a whole lot more. See https://datahub.io/core/country-codes


##### 
##### Random Seinfeld show quotes

`yada()`
>Random quote

`yadayada()`
>Seinfeld guessing game

